//
// Created by philipp on 16.10.18.
//

#include <nodelet/nodelet.h>
#include <ros/ros.h>

namespace car
{

/**
 * Nodelet which makes the car drive a left turn (steering angle is set to -10°) with a speed of one meter per second.
 */
class HelloDrive : public nodelet::Nodelet
{
public:
	void onInit() override;

private:
	ros::NodeHandle nh;

	ros::Publisher setThrottlePublisher;
	ros::Publisher setAnglePublisher;
};

}