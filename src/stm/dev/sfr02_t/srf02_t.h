/** *****************************************************************************************
 * Unit in charge:
 * @file	srf02_t.h
 * @author	wandji-kwuntchou
 * $Date:: 2015-02-20 20:30:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1197                  $
 * $Author:: wandji-kwuntchou   $
 *
 * ----------------------------------------------------------
 *
 * @brief A supersonic device with i2c attachment for the adaptive cruise control
 *
 ******************************************************************************************/

#ifndef SRF02_T_H_
#define SRF02_T_H_

/************* Includes *******************************************************************/
#include "per/i2c.h"
#include "sys/sigp/sigp.h"
#include "per/i2c.h"

#ifdef SRF02_T_EN

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/

/**
 * @brief	Initializes the chosen SRF02_t device
 *
 * @param[in,out] void
 *
 * @return void
 */
void srf02t_inititialize(void);

/**
 * @brief	start the measurement of SRF02 device by sending the write command
 *
 * @param[in,out] void
 *
 * @return void
 */
void srf02t_startMeasurement(void);

#endif /* SRF02_T_EN */
#endif /* SRF02_T_H_ */
