/** *****************************************************************************************
 * Unit in charge:
 * @file	wenc.c
 * @author	fklemm
 * $Date:: 2017-11-13 14:05:29 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2195              $
 * $Author:: fklemm   $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit adds triggered interrupts of the tacho signal from the encoder sensor and determines the
 * actual direction of each wheel while reading logic level at predefined GPIOS connected to the direction pin of the sensor.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "dev/wenc/wenc.h"
#include "per/eict.h"
#include "per/gpio.h"
#include "sys/util/io.h"
#include "app/config.h"

/************* Macros and constants *******************************************************/
#define MIN_TICKS_FOR_VALIDATION	  	10	// minimum ticks to trigger encoder validation
#define DRIFT_TOLERANCE					16 // tolerance for upcoming encoder drift
/************* Private typedefs ***********************************************************/
typedef struct
{
	int32_t wheelTicks;
	int8_t wheelDirection;
	uint8_t errorCounter;
} WheelEncoder;

typedef struct
{
	WheelEncoder encoderFL;
	WheelEncoder encoderFR;
	WheelEncoder encoderRL;
	WheelEncoder encoderRR;
	uint32_t carTicksUnsigned;
	int8_t carDirection;
	uint8_t encoderError;
} CarEncoder;

static gpio_t directionRR;
static gpio_t directionRL;

/************* Global variables ***********************************************************/
// output signals
int32_t* pWhlTicksFL_wenc;
int32_t* pWhlTicksFR_wenc;
int32_t* pWhlTicksRL_wenc;
int32_t* pWhlTicksRR_wenc;
int8_t* pCarDirection_wenc;

/************* Private static variables ***********************************************************/
static CarEncoder carEncoder;

/************* Private function prototypes ************************************************/
static uint8_t debounceEncoderError(WheelEncoder* encoder);
static void validateEncoderSignals(CarEncoder* carEncoder);

/************* Public functions ***********************************************************/
void wenc_init(void)
{
	// initialize external interrupt counter
	eict_init();

	// setup direction gpio for rear right encoder
	directionRR.channel = GPIO_CHAN_6;
	directionRR.mode = GPIO_MODE_IN;
	directionRR.state = GPIO_STATE_OFF;
	gpio_init(&directionRR);

	// setup direction gpio for rear left encoder
	directionRL.channel = GPIO_CHAN_9;
	directionRL.mode = GPIO_MODE_IN;
	directionRL.state = GPIO_STATE_OFF;
	gpio_init(&directionRL);

	*pWhlTicksFL_wenc = 0;
	*pWhlTicksFR_wenc = 0;
	*pWhlTicksRL_wenc = 0;
	*pWhlTicksRR_wenc = 0;
}

void wenc_step(void)
{
	// We'll get the wheel ticks from eict_getCounter() in odom directly and assume only to drive forwards.
}

/************* Private functions **********************************************************/

static uint8_t debounceEncoderError(WheelEncoder* encoder)
{
	uint8_t error = 0;
	// wheel ticks from encoder sensor
	if (encoder->wheelTicks > 0) {
		// debounce ticks from sensor
		if (encoder->errorCounter)
			encoder->errorCounter--;
		else
			error = 0;
		// there are no ticks from sensor
	} else {
		// debounce missing ticks from sensor
		if (encoder->errorCounter < DRIFT_TOLERANCE)
			encoder->errorCounter++;
		else
			error = 1;
	}
	return error;
}

static void validateEncoderSignals(CarEncoder* encoder)
{
	// if the car is moving
	// check each encoder sensor
	if (encoder->carTicksUnsigned > MIN_TICKS_FOR_VALIDATION) {
#ifdef FOUR_WHEEL_ODOM
		encoder->encoderError |= debounceEncoderError(&encoder->encoderFL);
		encoder->encoderError |= debounceEncoderError(&encoder->encoderFR);
#endif
		encoder->encoderError |= debounceEncoderError(&encoder->encoderRL);
		encoder->encoderError |= debounceEncoderError(&encoder->encoderRR);
		// car stand still
		// reset error counter
	} else {
#ifdef FOUR_WHEEL_ODOM
		encoder->encoderFL.errorCounter = 0;
		encoder->encoderFR.errorCounter = 0;
#endif
		encoder->encoderRL.errorCounter = 0;
		encoder->encoderRR.errorCounter = 0;
	}
	if (encoder->encoderError == 1) {
		io_printf("ERROR: wenc.c no valid Encoder data\n");
		encoder->encoderError = 0;
	}
}
