//
// Created by philipp on 09.07.18.
//

#include "utils/Config.h"
#include <fstream>

namespace car
{
namespace config
{

std::string directory()
{
	auto userHome = std::string{std::getenv("HOME")};
	return userHome + "/.car";
}

nlohmann::json open()
{
	auto configPath = directory() + "/config.json";
	std::ifstream file{configPath};
	nlohmann::json j;
	file >> j;
	return j;
}

}
}