//
// Created by philipp on 20.06.18.
//

#ifndef CAR_REMOTECONTROL_H
#define CAR_REMOTECONTROL_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include "car/RemoteAngle.h"
#include "car/RemoteThrottle.h"

namespace car
{

class RemoteControl : public nodelet::Nodelet
{
public:
	void onInit() override;

private:
	ros::NodeHandle nh;

	ros::Subscriber remoteThrottleSubscriber;
	ros::Subscriber remoteAngleSubscriber;

	ros::Publisher setThrottlePublisher;
	ros::Publisher setAnglePublisher;

	bool controlThrottle{false};
	bool controlSteeringAngle{false};

	void remoteThrottleCallback(const RemoteThrottle::ConstPtr & msg);

	void remoteAngleCallback(const RemoteAngle::ConstPtr & msg);

	void readConfig();
};

}

#endif //CAR_REMOTECONTROL_H
