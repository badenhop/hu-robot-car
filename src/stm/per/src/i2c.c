/** *****************************************************************************************
 * Unit in charge:
 * @file	i2c.c
 * @author	Hauke Petersen (mail@haukepetersen.de) Robert Ledwig (robert.ledwig@berner-mattner.de)
 * $Date:: 2015-09-07 14:20:54 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1668                  $
 * $Author:: hertel             $
 *
 * ----------------------------------------------------------
 *
 * @brief 		Driver for the stm32f4 i2c interfaces.
 *
 * 				The driver runs the i2c interfaces of stm32f4 controllers. The driver
 *				supports the target to be run in master as well as in slave mode. In
 *				slave mode it is up to the application designer to implement the slave
 *				behavior, e.g. simulate register wise data access. The driver
 * 				is designed to run synchronous calls, which means that write and
 * 				read operations are blocking until finished. The blocking time depends
 * 				hereby heavily on the selected bus frequency and the number of bytes to
 * 				be read or written.
 *
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include <string.h>
#include "brd/stm32f4xx_i2c.h"
#include "per/hwallocation.h"
#include "app/config.h"
#include "per/i2c.h"
#include "sys/util/stubs.h"

/************* Private typedefs ***********************************************************/
typedef struct
{
	I2C_TypeDef* device;			/* the I2C device used by the channel */
	uint8_t address;				/* set the cpu address, its set to 0 in master mode */
	volatile uint8_t busyFlag;   	/* set to 0 for not busy, 1 for reading, 2 for writing */
	uint8_t* pData;					/* memory location to read from / write into */
	uint16_t toGo;					/* number of bytes that still need to be read/written */
} channel_t;

/************* Macros and constants *******************************************************/
#define ADDR_SHIFT			1
#define ADDR_MASK			0xfe

#define READ				1
#define WRITE				2
#define READY				0

#define SENSORS_I2C         I2C1
#define I2C_SPEED           400000
#define I2C_OWN_ADDRESS     0x00

#define I2Cx_FLAG_TIMEOUT		((uint32_t) 900)
#define I2Cx_LONG_TIMEOUT       ((uint32_t) (300 * I2Cx_FLAG_TIMEOUT))

#define WAIT_FOR_FLAG(flag, value, timeout, errorcode)  I2CTimeout = timeout;\
          while(I2C_GetFlagStatus(SENSORS_I2C, flag) != value) {\
            if((I2CTimeout--) == 0) return I2Cx_TIMEOUT_UserCallback(errorcode); \
          }\

/************* Private static variables ***************************************************/
#ifdef I2C_CH1_EN
channel_t ch1;
#endif
#ifdef I2C_CH2_EN
channel_t ch2;
#endif
#ifdef I2C_CH3_EN
channel_t ch3;
#endif

static uint8_t RETRY_IN_MLSEC  = 55;
/************* Private function prototypes ************************************************/
/**
 * @brief	Interrupt handler for i2c communication
 *
 * @param[in,out] channel_t* channel contains the device, address, busyFlag, pData and number of bytes (togo)
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
void handleEvtIrq(channel_t* channel);

/**
 * @brief	Reset the local i2c channel structure to default values
 *
 * @param[in,out] channel_t* channel contains the device, address, busyFlag, pData and number of bytes (togo)
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
void resetChannel(channel_t* channel);

uint16_t GetI2cRetry(void);

static uint32_t I2Cx_TIMEOUT_UserCallback(char value);
/************* Public functions ***********************************************************/
int8_t i2c_init(i2c_t* i2c)
{
	/* temporary configuration variables */
	I2C_TypeDef* i2cDev;
	uint32_t i2cClk;
#ifdef I2C_INT_MODE
	uint8_t evtIrq;
	uint8_t errIrq;
#endif
	uint8_t i2cAf;
	uint16_t sdaPin;
	GPIO_TypeDef* sdaPort;
	uint32_t sdaClk;
	uint16_t sdaAfPin;
	uint16_t sclPin;
	GPIO_TypeDef* sclPort;
	uint32_t sclClk;
	uint16_t sclAfPin;

	if (i2c->mode == I2C_MASTER) {
		i2c->address = 0;
	}

	switch (i2c->channel) {
#ifdef I2C_CH1_EN
		case I2C_CH1:
			/* set defaults to channel 1 struct */
			memset(&ch1, 0, sizeof(channel_t));
			ch1.device = I2C1_DEV;
			/* setup channel 1 configuration */
			i2cDev = I2C1_DEV;
			i2cClk = I2C1_CLK;
#ifdef I2C_INT_MODE
			evtIrq = I2C1_EVT_IRQ;
			errIrq = I2C1_ERR_IRQ;
#endif
			i2cAf = I2C1_AF;
			sdaPin = I2C1_SDA_PIN;
			sdaPort = I2C1_SDA_PORT;
			sdaClk = I2C1_SDA_CLK;
			sdaAfPin = I2C1_SDA_AF_PIN;
			sclPin = I2C1_SCL_PIN;
			sclPort = I2C1_SCL_PORT;
			sclClk = I2C1_SCL_CLK;
			sclAfPin = I2C1_SCL_AF_PIN;
			break;
#endif
#ifdef I2C_CH2_EN
			case I2C_CH2:
			/* set defaults to channel 2 struct */
			memset(&ch2, 0, sizeof(channel_t));
			ch2.device = I2C2_DEV;
			/* setup channel 2 configuration */
			i2cDev = I2C2_DEV;
			i2cClk = I2C2_CLK;
#ifdef I2C_INT_MODE
			evtIrq = I2C2_EVT_IRQ;
			errIrq = I2C2_ERR_IRQ;
#endif
			i2cAf = I2C2_AF;
			sdaPin = I2C2_SDA_PIN;
			sdaPort = I2C2_SDA_PORT;
			sdaClk = I2C2_SDA_CLK;
			sdaAfPin = I2C2_SDA_AF_PIN;
			sclPin = I2C2_SCL_PIN;
			sclPort = I2C2_SCL_PORT;
			sclClk = I2C2_SCL_CLK;
			sclAfPin = I2C2_SCL_AF_PIN;
			break;
#endif
#ifdef I2C_CH3_EN
			case I2C_CH3:
			/* set defaults to channel 3 struct */
			memset(&ch3, 0, sizeof(channel_t));
			ch3.device = I2C3_DEV;
			/* setup channel 3 configuration */
			i2cDev = I2C3_DEV;
			i2cClk = I2C3_CLK;
			evtIrq = I2C3_EVT_IRQ;
			errIrq = I2C3_ERR_IRQ;
			i2cAf = I2C3_AF;
			sdaPin = I2C3_SDA_PIN;
			sdaPort = I2C3_SDA_PORT;
			sdaClk = I2C3_SDA_CLK;
			sdaAfPin = I2C3_SDA_AF_PIN;
			sclPin = I2C3_SCL_PIN;
			sclPort = I2C3_SCL_PORT;
			sclClk = I2C3_SCL_CLK;
			sclAfPin = I2C3_SCL_AF_PIN;
			break;
#endif
	}

	/* enable module clocks */
	RCC_AHB1PeriphClockCmd(sdaClk, ENABLE);
	RCC_AHB1PeriphClockCmd(sclClk, ENABLE);
	RCC_APB1PeriphClockCmd(i2cClk, ENABLE);

	/* configure i2c pins */
	GPIO_InitTypeDef pinInit;
	pinInit.GPIO_Mode = GPIO_Mode_AF;
	pinInit.GPIO_Speed = GPIO_Speed_100MHz;
	pinInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
	pinInit.GPIO_OType = GPIO_OType_OD;
	pinInit.GPIO_Pin = sdaPin;
	GPIO_Init(sdaPort, &pinInit);
	pinInit.GPIO_Pin = sclPin;
	GPIO_Init(sclPort, &pinInit);
	GPIO_PinAFConfig(sdaPort, sdaAfPin, i2cAf);
	GPIO_PinAFConfig(sclPort, sclAfPin, i2cAf);

	/* init I2C device */
	I2C_InitTypeDef i2cInit;
	I2C_DeInit(i2cDev);
	i2cInit.I2C_Ack = I2C_Ack_Enable;
	i2cInit.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	i2cInit.I2C_ClockSpeed = i2c->frequency;
	i2cInit.I2C_DutyCycle = I2C_DutyCycle_2;
	i2cInit.I2C_Mode = I2C_Mode_I2C;
	i2cInit.I2C_OwnAddress1 = i2c->address;
	I2C_Init(i2cDev, &i2cInit);

#ifdef I2C_INT_MODE
	/* configure i2c interrupts */
	NVIC_InitTypeDef nvicInit;
	nvicInit.NVIC_IRQChannel = evtIrq;
	nvicInit.NVIC_IRQChannelPreemptionPriority = 0x0;
	nvicInit.NVIC_IRQChannelSubPriority = 0x0;
	nvicInit.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicInit);

	nvicInit.NVIC_IRQChannel = errIrq;
	nvicInit.NVIC_IRQChannelPreemptionPriority = 0x0;
	nvicInit.NVIC_IRQChannelSubPriority = 0x0;
	nvicInit.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicInit);

	/* init I2C device */
	I2C_InitTypeDef i2cInit;
	I2C_DeInit(i2cDev);
	i2cInit.I2C_Ack = I2C_Ack_Enable;
	i2cInit.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	i2cInit.I2C_ClockSpeed = i2c->frequency;
	i2cInit.I2C_DutyCycle = I2C_DutyCycle_2;
	i2cInit.I2C_Mode = I2C_Mode_I2C;
	i2cInit.I2C_OwnAddress1 = i2c->address;
	I2C_Init(i2cDev, &i2cInit);

	/* enable interrupts for event and buffer */
	I2C_ITConfig(i2cDev, I2C_IT_EVT, ENABLE);
	I2C_ITConfig(i2cDev, I2C_IT_BUF, ENABLE);
	I2C_ITConfig(i2cDev, I2C_IT_ERR, ENABLE);
#endif
	return I2C_OK;
}

int8_t i2c_enable(i2c_channel_t channel)
{
	switch (channel) {
#ifdef I2C_CH1_EN
		case I2C_CH1:
			I2C_Cmd(I2C1_DEV, ENABLE);
			break;
#endif
#ifdef I2C_CH2_EN
			case I2C_CH2:
			I2C_Cmd(I2C2_DEV, ENABLE);
			break;
#endif
#ifdef I2C_CH3_EN
			case I2C_CH3:
			I2C_Cmd(I2C3_DEV, ENABLE);
			break;
#endif
	}
	return I2C_OK;
}

int8_t i2c_disable(i2c_channel_t channel)
{
	switch (channel) {
#ifdef I2C_CH1_EN
		case I2C_CH1:
			I2C_Cmd(I2C1_DEV, DISABLE);
			break;
#endif
#ifdef I2C_CH2_EN
			case I2C_CH2:
			I2C_Cmd(I2C2_DEV, DISABLE);
			break;
#endif
#ifdef I2C_CH3_EN
			case I2C_CH3:
			I2C_Cmd(I2C3_DEV, DISABLE);
			break;
#endif
	}
	return I2C_OK;
}

int32_t i2c_readRaw(i2c_channel_t channel, uint8_t address, uint8_t* pData, uint16_t numOfBytes)
{
	channel_t* dev;
	uint8_t i;
	switch (channel) {
#ifdef I2C_CH1_EN
		case I2C_CH1:
			dev = &ch1;
			break;
#endif
#ifdef I2C_CH2_EN
			case I2C_CH2:
			dev = &ch2;
			break;
#endif
#ifdef I2C_CH3_EN
			case I2C_CH3:
			dev = &ch3;
			break;
#endif
	}
	/* set the state data for the channel */
	dev->address = address;
	dev->busyFlag = READ;
	dev->pData = pData;
	dev->toGo = numOfBytes;
	__IO uint32_t  I2CTimeout = I2Cx_LONG_TIMEOUT;

	WAIT_FOR_FLAG(I2C_FLAG_BUSY, RESET, I2Cx_LONG_TIMEOUT, 7);
	I2C_GenerateSTART(dev->device, ENABLE);
	WAIT_FOR_FLAG(I2C_FLAG_SB, SET, I2Cx_FLAG_TIMEOUT, 8);
	I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Transmitter); /* Send address from which registers will be read */
	WAIT_FOR_FLAG(I2C_FLAG_ADDR, SET, I2Cx_FLAG_TIMEOUT, 9);
	I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
	I2C_ReadRegister(dev->device, I2C_Register_SR2 );
	WAIT_FOR_FLAG(I2C_FLAG_TXE, SET, I2Cx_FLAG_TIMEOUT, 10);
	I2C_SendData(dev->device, dev->pData[0]);
	WAIT_FOR_FLAG(I2C_FLAG_TXE, SET, I2Cx_FLAG_TIMEOUT, 11);
	/* send the start bit a second time*/
	I2C_GenerateSTART(dev->device, ENABLE);
	WAIT_FOR_FLAG(I2C_FLAG_SB, SET, I2Cx_FLAG_TIMEOUT, 12);
	I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Receiver);
	WAIT_FOR_FLAG(I2C_FLAG_ADDR, SET, I2Cx_FLAG_TIMEOUT, 13);
	numOfBytes--;
	if(numOfBytes == 1) {
		I2C_AcknowledgeConfig(dev->device, DISABLE);
		I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
		I2C_ReadRegister(dev->device, I2C_Register_SR2 );
		I2C_GenerateSTOP(dev->device, ENABLE);
		WAIT_FOR_FLAG(I2C_FLAG_RXNE, SET, I2Cx_FLAG_TIMEOUT, 14);
		dev->pData[0] = I2C_ReceiveData(dev->device);
	} else if(numOfBytes == 2) {
		I2C_AcknowledgeConfig(dev->device, DISABLE);
		dev->device->CR1 |= I2C_CR1_POS;
		I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
		I2C_ReadRegister(dev->device, I2C_Register_SR2 );
		WAIT_FOR_FLAG(I2C_FLAG_BTF, SET, I2Cx_FLAG_TIMEOUT, 15);
		I2C_GenerateSTOP(dev->device, ENABLE);
		dev->pData[0] = I2C_ReceiveData(dev->device);
		dev->pData[1] = I2C_ReceiveData(dev->device);
	} else if(numOfBytes == 3) {
		I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
		I2C_ReadRegister(dev->device, I2C_Register_SR2 );
		WAIT_FOR_FLAG(I2C_FLAG_BTF, SET, I2Cx_FLAG_TIMEOUT, 16);
		I2C_AcknowledgeConfig(dev->device, DISABLE);
		dev->pData[0] = I2C_ReceiveData(dev->device);
		I2C_GenerateSTOP(dev->device, ENABLE);
		dev->pData[1] = I2C_ReceiveData(dev->device);
		WAIT_FOR_FLAG(I2C_FLAG_RXNE, SET, I2Cx_FLAG_TIMEOUT, 17);
		dev->pData[2] = I2C_ReceiveData(dev->device);
	} else {
		I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
		I2C_ReadRegister(dev->device, I2C_Register_SR2 );
		for(i=0;i<numOfBytes;i++) {
			if(i==(numOfBytes-3)) {
				WAIT_FOR_FLAG(I2C_FLAG_BTF, SET, I2Cx_FLAG_TIMEOUT, 16);
				I2C_AcknowledgeConfig(dev->device, DISABLE);
				dev->pData[i++] = I2C_ReceiveData(dev->device);
				I2C_GenerateSTOP(dev->device, ENABLE);
				dev->pData[i++] = I2C_ReceiveData(dev->device);
				WAIT_FOR_FLAG(I2C_FLAG_RXNE, SET, I2Cx_FLAG_TIMEOUT, 17);
				dev->pData[i++] = I2C_ReceiveData(dev->device);
				goto endReadLoop;
			}
			WAIT_FOR_FLAG(I2C_FLAG_RXNE, SET, I2Cx_FLAG_TIMEOUT, 18);
			dev->pData[i] = I2C_ReceiveData(dev->device);
		}
	}
endReadLoop:
	I2C_ClearFlag(dev->device, I2C_FLAG_BTF);
	WAIT_FOR_FLAG(I2C_FLAG_BUSY, RESET, I2Cx_LONG_TIMEOUT, 19);
	I2C_AcknowledgeConfig(dev->device, ENABLE);
	dev->device->CR1 &= ~I2C_CR1_POS;

	return I2C_OK;
}

int32_t i2c_readReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* pData, uint16_t numOfBytes)
{
	uint16_t retryInMlsec = GetI2cRetry();
	uint8_t retries = 0;
	int8_t ret = 0;
	uint8_t data[numOfBytes + 1];
	data[0] = reg;
	memcpy(data + 1, pData, numOfBytes);

tryWriteAgain:
	ret = 0;
	ret = i2c_readRaw(channel, address, data, numOfBytes + 1);
	memcpy(pData, data, numOfBytes);

	if(ret && retryInMlsec) {
		if(retries++ > 4) {
			return ret;
		}
		/* delay leads to crash */
		delay(retryInMlsec);
		goto tryWriteAgain;
	}
	return ret;
}

int32_t i2c_writeRaw(i2c_channel_t channel, uint8_t address, uint8_t* pData, uint16_t numOfBytes)
{
	channel_t* dev;
	volatile uint8_t i;
	switch (channel) {
#ifdef I2C_CH1_EN
		case I2C_CH1:
			dev = &ch1;
			break;
#endif
#ifdef I2C_CH2_EN
			case I2C_CH2:
			dev = &ch2;
			break;
#endif
#ifdef I2C_CH3_EN
			case I2C_CH3:
			dev = &ch3;
			break;
#endif
	}

	/* set the state data for the channel */
	dev->address = address;
	dev->busyFlag = WRITE;
	dev->pData = pData;
	dev->toGo = numOfBytes;
	__IO uint32_t  I2CTimeout = I2Cx_LONG_TIMEOUT;

	WAIT_FOR_FLAG(I2C_FLAG_BUSY, RESET, I2Cx_LONG_TIMEOUT, 1);
	I2C_GenerateSTART(dev->device, ENABLE);
	WAIT_FOR_FLAG(I2C_FLAG_SB, SET, I2Cx_FLAG_TIMEOUT, 2);
	I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Transmitter);
	WAIT_FOR_FLAG(I2C_FLAG_ADDR, SET, I2Cx_FLAG_TIMEOUT, 3);
	I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
	I2C_ReadRegister(dev->device, I2C_Register_SR2 );
	WAIT_FOR_FLAG(I2C_FLAG_TXE, SET, I2Cx_FLAG_TIMEOUT, 4);
	I2C_SendData(dev->device, dev->pData[0]);
	for(i=1;i<numOfBytes;i++) {
		WAIT_FOR_FLAG(I2C_FLAG_TXE, SET, I2Cx_FLAG_TIMEOUT, 5);
		I2C_SendData(dev->device, dev->pData[i]);
	}
	WAIT_FOR_FLAG(I2C_FLAG_BTF, SET, I2Cx_FLAG_TIMEOUT, 6);
	I2C_GenerateSTOP(dev->device, ENABLE);
	return I2C_OK;
}

int32_t i2c_writeReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* pData, uint16_t numOfBytes)
{
	uint16_t retryInMlsec = GetI2cRetry();
	uint8_t retries = 0;
	int8_t ret = 0;
	uint8_t data[numOfBytes + 1];
	data[0] = reg;
	memcpy(data + 1, pData, numOfBytes);
tryWriteAgain:
	ret = 0;
	ret = i2c_writeRaw(channel, address, data, numOfBytes + 1);

	if(ret && retryInMlsec) {
		if(retries++ > 4) {
			return ret;
		}
		delay(retryInMlsec);
		goto tryWriteAgain;
	}

	return ret;
}



/************* Private functions **********************************************************/

/* interrupt handler */
#ifdef I2C_CH1_EN
#ifdef affe
void I2C1_EV_IRQHandler(void)
{
	handleEvtIrq(&ch1);
}

uint32_t I2C1_ER_IRQHandler(void)
{
	I2C_GenerateSTOP(ch1.device, ENABLE);
	ch1.busyFlag = 0;

	I2C_ClearFlag(ch1.device, I2C_FLAG_AF );
	I2C_ClearFlag(ch1.device, I2C_FLAG_ARLO );
	I2C_ClearFlag(ch1.device, I2C_FLAG_BERR );

	return I2C_ERR;
}
#endif
#endif

#ifdef I2C_CH2_EN
void I2C2_EV_IRQHandler(void)
{
	handleEvtIrq(&ch2);

}

void I2C2_ER_IRQHandler(void)
{
	I2C_GenerateSTOP(ch2.device, ENABLE);
	ch2.busyFlag = 0;

	I2C_ClearFlag(ch2.device, I2C_FLAG_AF);
	I2C_ClearFlag(ch2.device, I2C_FLAG_ARLO);
	I2C_ClearFlag(ch2.device, I2C_FLAG_BERR);

	return I2C_ERR;
}
#endif

#ifdef I2C_CH3_EN
void I2C3_EV_IRQHandler(void)
{
	handleEvtIrq(&ch3);
}

void I2C3_ER_IRQHandler(void)
{
	I2C_GenerateSTOP(ch3.device, ENABLE);
	ch3.busyFlag = 0;

	I2C_ClearFlag(ch3.device, I2C_FLAG_AF);
	I2C_ClearFlag(ch3.device, I2C_FLAG_ARLO);
	I2C_ClearFlag(ch3.device, I2C_FLAG_BERR);

	return I2C_ERR;
}
#endif
#ifdef affe
void handleEvtIrq(channel_t* dev)
{
	if (I2C_GetFlagStatus(dev->device, I2C_FLAG_SB ) == SET) {    /* send address after start bit */
			if (dev->busyFlag == READ) {
				I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Receiver );
			} else if (dev->busyFlag == WRITE) {
				I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Transmitter );
			}
		} else if (I2C_GetFlagStatus(dev->device, I2C_FLAG_ADDR ) == SET	/* address send or byte transfer finished */
		|| I2C_GetFlagStatus(dev->device, I2C_FLAG_BTF ) == SET) {
			I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
			I2C_ReadRegister(dev->device, I2C_Register_SR2 );
			if (dev->busyFlag == WRITE) {						 /* write data */
				/* sending out data after address was send */
				if (dev->toGo > 0) {
					I2C_SendData(dev->device, dev->pData[0]);
					--dev->toGo;
					++dev->pData;
				} else {
					I2C_GenerateSTOP(dev->device, ENABLE);
					resetChannel(dev);
				}

			}
		} else if (I2C_GetFlagStatus(dev->device, I2C_FLAG_RXNE ) == SET) {    /* data register not empty -> read */
			/* read data from the address that is provided */
			I2C_ReadRegister(dev->device, I2C_Register_SR1 );
			I2C_ReadRegister(dev->device, I2C_Register_SR2 );
			if (dev->busyFlag == READ) {								/* read data */
				--dev->toGo;
				if (dev->toGo == 1) {
					I2C_AcknowledgeConfig(dev->device, DISABLE);
				}
				dev->pData[0] = I2C_ReceiveData(dev->device);

				++dev->pData;
				if (dev->toGo == 0) {
					resetChannel(dev);
					I2C_GenerateSTOP(dev->device, ENABLE);
				}
			}
		}

	int8_t i, timeout=0;

	while(I2C_GetFlagStatus(dev->device, I2C_FLAG_SB) != SET);

	if (dev->busyFlag == READ) {
		I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Transmitter); /* Send address from which registers will be read */
		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_ADDR) != SET);
		I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
		I2C_ReadRegister(dev->device, I2C_Register_SR2 );
		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_TXE) != SET);
		I2C_SendData(dev->device, dev->pData[0]);
		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_TXE) != SET);
		/* send the start bit a second time*/
		I2C_GenerateSTART(dev->device, ENABLE);
		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_SB) != SET);
		I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Receiver);
		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_ADDR) != SET);
		if(--dev->toGo == 1) {
			I2C_AcknowledgeConfig(dev->device, DISABLE);
			I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
			I2C_ReadRegister(dev->device, I2C_Register_SR2 );
			I2C_GenerateSTOP(dev->device, ENABLE);
			while(I2C_GetFlagStatus(dev->device, I2C_FLAG_RXNE) != SET);
			dev->pData[0] = I2C_ReceiveData(dev->device);
		} else if(--dev->toGo == 2) {
			I2C_AcknowledgeConfig(dev->device, DISABLE);
			dev->device->CR1 |= I2C_CR1_POS;
			I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
			I2C_ReadRegister(dev->device, I2C_Register_SR2 );
			while(I2C_GetFlagStatus(dev->device, I2C_FLAG_BTF) != SET);
			I2C_GenerateSTOP(dev->device, ENABLE);
			dev->pData[0] = I2C_ReceiveData(dev->device);
			dev->pData[1] = I2C_ReceiveData(dev->device);
		} else if(--dev->toGo == 3) {
			I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
			I2C_ReadRegister(dev->device, I2C_Register_SR2 );
			while(I2C_GetFlagStatus(dev->device, I2C_FLAG_BTF) != SET);
			I2C_AcknowledgeConfig(dev->device, DISABLE);
			dev->pData[0] = I2C_ReceiveData(dev->device);
			I2C_GenerateSTOP(dev->device, ENABLE);
			dev->pData[1] = I2C_ReceiveData(dev->device);
			while(I2C_GetFlagStatus(dev->device, I2C_FLAG_RXNE) != SET);
			dev->pData[2] = I2C_ReceiveData(dev->device);
		} else {
			I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
			I2C_ReadRegister(dev->device, I2C_Register_SR2 );

			for(i=0;i<(dev->toGo)-1;i++) {
				if(i==((dev->toGo)-4)) {
					while(I2C_GetFlagStatus(dev->device, I2C_FLAG_BTF) != SET);
					I2C_AcknowledgeConfig(dev->device, DISABLE);
					dev->pData[i++] = I2C_ReceiveData(dev->device);
					I2C_GenerateSTOP(dev->device, ENABLE);
					dev->pData[i++] = I2C_ReceiveData(dev->device);
					while(I2C_GetFlagStatus(dev->device, I2C_FLAG_RXNE) != SET);
					dev->pData[i++] = I2C_ReceiveData(dev->device);
					goto endReadLoop;
				}
				while(I2C_GetFlagStatus(dev->device, I2C_FLAG_RXNE) != SET);
				dev->pData[i] = I2C_ReceiveData(dev->device);
			}
		}
	endReadLoop:
		I2C_ClearFlag(dev->device, I2C_FLAG_BTF);
		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_BUSY) != RESET);
		I2C_AcknowledgeConfig(dev->device, ENABLE);
		dev->device->CR1 &= ~I2C_CR1_POS;

	} else if (dev->busyFlag == WRITE) {
		I2C_Send7bitAddress(dev->device, dev->address, I2C_Direction_Transmitter);

		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_ADDR) != SET) {
			if((timeout++) == 2000) {
				io_printf("I2C Fail\n\r");
			}
		}
		I2C_ReadRegister(dev->device, I2C_Register_SR1 );    /* reset address send flag (reading SR1 followed by SR2) */
		I2C_ReadRegister(dev->device, I2C_Register_SR2 );

		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_TXE) != SET);
		I2C_SendData(dev->device, dev->pData[0]);
		for(i=0;i<(dev->toGo)-1;i++) {
			--dev->toGo;
			++dev->pData;
			while(I2C_GetFlagStatus(dev->device, I2C_FLAG_TXE) != SET);
			I2C_SendData(dev->device, dev->pData[0]);
		}

		while(I2C_GetFlagStatus(dev->device, I2C_FLAG_BTF) != SET);
		I2C_GenerateSTOP(dev->device, ENABLE);
//		resetChannel(dev);
	}
//	resetChannel(dev);

}

void resetChannel(channel_t* channel)
{
	channel->address = 0;
	channel->busyFlag = READY;
	channel->pData = NULL;
	channel->toGo = 0;
}
#endif
static uint32_t I2Cx_TIMEOUT_UserCallback(char value) {

  /* The following code allows I2C error recovery and return to normal communication
     if the error source doesn�t still exist (ie. hardware issue..) */
  I2C_InitTypeDef i2cInit;

  I2C_GenerateSTOP(SENSORS_I2C, ENABLE);
  I2C_SoftwareResetCmd(SENSORS_I2C, ENABLE);
  I2C_SoftwareResetCmd(SENSORS_I2C, DISABLE);

  I2C_DeInit(SENSORS_I2C);
  i2cInit.I2C_Mode = I2C_Mode_I2C;
  i2cInit.I2C_DutyCycle = I2C_DutyCycle_2;
  i2cInit.I2C_OwnAddress1 = I2C_OWN_ADDRESS;
  i2cInit.I2C_Ack = I2C_Ack_Enable;
  i2cInit.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  i2cInit.I2C_ClockSpeed = I2C_SPEED;

  /* Enable the I2C peripheral */
  I2C_Cmd(SENSORS_I2C, ENABLE);

  /* Initialize the I2C peripheral */
  I2C_Init(SENSORS_I2C, &i2cInit);

  return 1;
}

void SetI2cRetry(uint8_t mlSec) {
	RETRY_IN_MLSEC = mlSec;
}

uint16_t GetI2cRetry() {
	return RETRY_IN_MLSEC;
}
