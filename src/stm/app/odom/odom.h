/** *****************************************************************************************
 * Unit in charge:
 * @file	odom.h
 * @author	Neumerkel
 * $Date:: 2017-10-23 17:51:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2169              $
 * $Author::    		$
 *
 * ----------------------------------------------------------
 *
 * @brief The odometry module calculates the vehicles instantaneous x and y
 * coordinates relative to its initial starting point and the actual vehicle speed out of
 * incremental wheel encoder ticks.
 *
 ******************************************************************************************/


#ifndef ODOM_H_
#define ODOM_H_

/************* Includes *******************************************************************/
#include <math.h>
#include "brd/startup/stm32f4xx.h"
#include "sys/sigp/sigp.h"
#include "per/irq.h"
#include "sys/systime/systime.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/
/**
 *	@brief The init function needs to be called during the initialization of the system.
 *	It sets all local variables to their initial values.
 */
int8_t odom_init(void);

/**
 *	@brief The step function needs to be called cyclically by the scheduler. It evaluates
 *	the input signals and writes the determined values in the signal pool.
 */
void odom_step(void);

#endif /* ODOM_H_ */
