/** *****************************************************************************************
 * Unit in charge:
 * @file	stangproc.h
 * @author	hinze
 * $Date:: 2017-09-06 12:43:27 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2158                  $
 * $Author:: mbrieske    $
 *
 * ----------------------------------------------------------
 *
 * @brief The torque processing transforms the valid torque target value into the expected format and range of the actuating module.
 *
 ******************************************************************************************/

#ifndef STANGPROC_H_
#define STANGPROC_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "dev/servo/servo.h"
#include "per/irq.h"
#include "sys/systime/systime.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the steering angle processing
 *
 * @param [in,out] void
 *
 * @return 0
 */
int8_t stangproc_init(void);

/**
 * @brief This function process the steering angle provided by application
 * steering angle target selector in a angle that can be read by the servo.
 * The process consists in an linear transformation
 *
 * @param [in,out] void
 *
 * @return void
 */
void stangproc_step(void);

#endif /* STANGPROC_H_ */
