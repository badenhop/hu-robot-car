/** *****************************************************************************************
 * Unit in charge:
 * @file	eeprom_25LC080A.c
 * @author	Adrian
 * $Date:: 2017-06-27 11:34:20 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev::                   $
 * $Author::           $
 *
 * ----------------------------------------------------------
 *
 * @brief
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../eeprom_25LC080A.h"
#include "brd/startup/stm32f4xx.h"
#include "sys/util/stubs.h"
#include "sys/util/io.h"
#include "../../../per/spi.h"
/************* Macros and constants *******************************************************/
#define SET_CS()				(GPIOD->BSRRH |= GPIO_Pin_3); delay(1);
#define RESET_CS()				delay(1);(GPIOD->BSRRL |= GPIO_Pin_3);delay(10);
#define WAIT_FOR_EEPROM_RDY() 	while(eeprom_getWorkInProgress())delay(100);

//opcodes
#define LAST_ADDRESS 0x03FF
#define PAGESIZE 16
#define READ  0b00000011
#define WRITE 0b00000010
#define WRDI  0b00000100
#define WREN  0b00000110
#define RDSR  0b00000101
#define WRSR  0b00000001

/************* Private static variables ***************************************************/

/************* Private function prototypes ************************************************/
uint8_t eeprom_getWorkInProgress();
/************* Public functions ***********************************************************/
int8_t eeprom_init() {

	GPIO_InitTypeDef GPIO_InitStruct;

	// enable clock for used IO pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* Configure the chip select, Hold and Write Protect pins
	 * PD3 = CS
	 * PD7 = HOLD
	 * PD6 = WRITE_PROTECT
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_7 | GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOD, &GPIO_InitStruct);

	GPIOD->BSRRL |= GPIO_Pin_7; // set HOLD high
	GPIOD->BSRRL |= GPIO_Pin_3; // set CS high
	GPIOD->BSRRH |= GPIO_Pin_6; // reset WRITE_PROTECT

	SPI2_init();

	return 0;
}

uint8_t eeprom_writeByte(uint16_t address, uint8_t data) {
	uint8_t writtenBytes = 0;
	if (address <= LAST_ADDRESS) {
		SET_CS();
		SPI2_sendByte(WREN);
		RESET_CS();
		delay(10);
		SET_CS();
		SPI2_sendByte(WRITE);
		SPI2_sendByte((uint8_t) (address >> 8)); //send MSByte address first
		SPI2_sendByte((uint8_t) (address & 0x00FF)); //send LSByte address
		SPI2_sendByte(data); //write data byte
		RESET_CS();
		writtenBytes = 1;
	} else {
		/* address out of memory area */
		writtenBytes = 0;
	}
	return writtenBytes;
}

/**
 * return number of written bytes
 */
uint8_t eeprom_writeBuffer(uint16_t address, uint8_t* dataBuffer,
		uint8_t bufferLength) {

		uint8_t bytesToSend = bufferLength;

		if ((dataBuffer != NULL) && (bufferLength > 0)
			&& (address + bufferLength < LAST_ADDRESS)) {

		uint8_t dataWritten = 0;
		uint8_t bytesToPageEnd = PAGESIZE - (address % PAGESIZE); /* note that u cant write over page side, read Datasheet 25LC080A */

		WAIT_FOR_EEPROM_RDY();
		SET_CS();
		SPI2_sendByte(WREN);
		RESET_CS();
		SET_CS();
		SPI2_sendByte(WRITE);
		SPI2_sendByte((uint8_t) (address >> 8)); //send MSByte address first
		SPI2_sendByte((uint8_t) (address & 0x00FF)); //send LSByte address
		if (bytesToPageEnd > bytesToSend) {
			/* no collision with page end so we easily can write */
			dataWritten = SPI2_sendBuffer(dataBuffer, bytesToSend);
		} else {
			/* collision with page end so we have to split writing */
			dataWritten = SPI2_sendBuffer(dataBuffer, bytesToPageEnd);
		}
		RESET_CS();
		address += dataWritten;
		dataBuffer += dataWritten;
		bytesToSend -= dataWritten;
		/* 2. now we are at page start and can write full 16byte blocks */
		while (bytesToSend > 0) {

			WAIT_FOR_EEPROM_RDY();
			delay(10);
			SET_CS();
			SPI2_sendByte(WREN);
			RESET_CS();
			SET_CS();
			SPI2_sendByte(WRITE);
			SPI2_sendByte((uint8_t) (address >> 8)); //send MSByte address first
			SPI2_sendByte((uint8_t) (address & 0x00FF)); //send LSByte address
			if (bytesToSend > PAGESIZE) {
				/* just send Blocks of 16 bytes and then "restart" write Process */
				dataWritten = SPI2_sendBuffer(dataBuffer, PAGESIZE);
			} else {
				dataWritten = SPI2_sendBuffer(dataBuffer, bytesToSend);
			}
			RESET_CS();
			address += dataWritten;
			dataBuffer += dataWritten;
			bytesToSend -= dataWritten;
		}
	} else {
		bytesToSend = 0;
	}

	return (bufferLength - bytesToSend);
}

/**
 * return: byte from address in eeprom
 */
uint8_t eeprom_readByte(uint16_t address) {
	uint8_t data = 0;
	if (address < LAST_ADDRESS) {
		//READ EEPROM
		SET_CS();
		SPI2_sendByte(READ); //transmit read opcode
		SPI2_sendByte((uint8_t) (address >> 8)); //send MSByte address first
		SPI2_sendByte((uint8_t) (address & 0x00FF)); //send LSByte address
		data = SPI2_readByte(0xFF); //get data byte
		RESET_CS();
	} else {
		data = 0;
	}
	return data;
}

/**
 * return: number of byte reads
 */
uint8_t eeprom_readBuffer(uint16_t address, uint8_t* dataBuffer,
		uint8_t bufferLength) {
		uint8_t dataReads = 0;
	if ((dataBuffer != NULL) && (bufferLength > 0)
			&& (address + bufferLength < LAST_ADDRESS)) {
		SET_CS();
		SPI2_sendByte(READ); //transmit read opcode
		SPI2_sendByte((uint8_t) (address >> 8)); //send MSByte address first
		SPI2_sendByte((uint8_t) (address & 0x00FF)); //send LSByte address
		for (dataReads = 0; dataReads < bufferLength; dataReads++) {
			dataBuffer[dataReads] = SPI2_readByte(); //get data byte
		}
		RESET_CS();
	} else {
		dataReads = 0;
	}
	return dataReads;
}

uint8_t eeprom_getWorkInProgress() {
	uint8_t data;
	SET_CS();
	SPI2_sendByte(RDSR); // send RDSR command
	data = SPI2_readByte(0xFF); //get data byte
	RESET_CS();
	return (data & (1 << 0));
}
