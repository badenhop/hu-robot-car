#pragma once
// MESSAGE CMD_REQUEST_STATECHANGE PACKING

#define MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE 101

MAVPACKED(
typedef struct __mavlink_cmd_request_statechange_t {
 uint8_t state; /*< reqeusted state*/
}) mavlink_cmd_request_statechange_t;

#define MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN 1
#define MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN 1
#define MAVLINK_MSG_ID_101_LEN 1
#define MAVLINK_MSG_ID_101_MIN_LEN 1

#define MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC 229
#define MAVLINK_MSG_ID_101_CRC 229



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CMD_REQUEST_STATECHANGE { \
    101, \
    "CMD_REQUEST_STATECHANGE", \
    1, \
    {  { "state", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cmd_request_statechange_t, state) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CMD_REQUEST_STATECHANGE { \
    "CMD_REQUEST_STATECHANGE", \
    1, \
    {  { "state", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cmd_request_statechange_t, state) }, \
         } \
}
#endif

/**
 * @brief Pack a cmd_request_statechange message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param state reqeusted state
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_request_statechange_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t state)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN];
    _mav_put_uint8_t(buf, 0, state);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN);
#else
    mavlink_cmd_request_statechange_t packet;
    packet.state = state;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
}

/**
 * @brief Pack a cmd_request_statechange message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param state reqeusted state
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_request_statechange_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t state)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN];
    _mav_put_uint8_t(buf, 0, state);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN);
#else
    mavlink_cmd_request_statechange_t packet;
    packet.state = state;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
}

/**
 * @brief Encode a cmd_request_statechange struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cmd_request_statechange C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_request_statechange_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cmd_request_statechange_t* cmd_request_statechange)
{
    return mavlink_msg_cmd_request_statechange_pack(system_id, component_id, msg, cmd_request_statechange->state);
}

/**
 * @brief Encode a cmd_request_statechange struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cmd_request_statechange C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_request_statechange_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cmd_request_statechange_t* cmd_request_statechange)
{
    return mavlink_msg_cmd_request_statechange_pack_chan(system_id, component_id, chan, msg, cmd_request_statechange->state);
}

/**
 * @brief Send a cmd_request_statechange message
 * @param chan MAVLink channel to send the message
 *
 * @param state reqeusted state
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cmd_request_statechange_send(mavlink_channel_t chan, uint8_t state)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN];
    _mav_put_uint8_t(buf, 0, state);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE, buf, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
#else
    mavlink_cmd_request_statechange_t packet;
    packet.state = state;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE, (const char *)&packet, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
#endif
}

/**
 * @brief Send a cmd_request_statechange message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_cmd_request_statechange_send_struct(mavlink_channel_t chan, const mavlink_cmd_request_statechange_t* cmd_request_statechange)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_cmd_request_statechange_send(chan, cmd_request_statechange->state);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE, (const char *)cmd_request_statechange, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
#endif
}

#if MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_cmd_request_statechange_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t state)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, state);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE, buf, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
#else
    mavlink_cmd_request_statechange_t *packet = (mavlink_cmd_request_statechange_t *)msgbuf;
    packet->state = state;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE, (const char *)packet, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_CRC);
#endif
}
#endif

#endif

// MESSAGE CMD_REQUEST_STATECHANGE UNPACKING


/**
 * @brief Get field state from cmd_request_statechange message
 *
 * @return reqeusted state
 */
static inline uint8_t mavlink_msg_cmd_request_statechange_get_state(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a cmd_request_statechange message into a struct
 *
 * @param msg The message to decode
 * @param cmd_request_statechange C-struct to decode the message contents into
 */
static inline void mavlink_msg_cmd_request_statechange_decode(const mavlink_message_t* msg, mavlink_cmd_request_statechange_t* cmd_request_statechange)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    cmd_request_statechange->state = mavlink_msg_cmd_request_statechange_get_state(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN? msg->len : MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN;
        memset(cmd_request_statechange, 0, MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE_LEN);
    memcpy(cmd_request_statechange, _MAV_PAYLOAD(msg), len);
#endif
}
