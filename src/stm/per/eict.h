
/** *****************************************************************************************
 * Unit in charge:
 * @file	eict.h
 * @author	Yoga
 * $Date:: 2015-05-05 13:31:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev::                   $
 * $Author::           $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit configures an input capture timer triggering on the edges of the wheel encoder.
 *
 ******************************************************************************************/


#ifndef EICT_H_
#define EICT_H_

/************* Includes *******************************************************************/
#include "per/hwallocation.h"

/************* Public typedefs ************************************************************/

/************* Privat typedefs ************************************************************/

/************* Macros and constants *******************************************************/
/************* Public function prototypes *************************************************/
void eict_init(void);

void eict_getCounter(uint16_t *frontLeftEnc, uint16_t *frontRightEnc, uint16_t *rearLeftEnc, uint16_t *rearRightEnc);

void eict_getCounter2(uint16_t *rearLeftEnc, uint16_t *rearRightEnc);

#endif /* EICT_H_ */
