/** *****************************************************************************************
 * Unit in charge:
 * @file	config.h
 * @author	SONSKI
 * $Date:: 2016-12-08 14:01:40 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2061                  $
 * $Author:: adelhoefer         $
 * 
 * ----------------------------------------------------------
 *
 * @brief Defines modules and module elements used and therefore compiled
 *
 ******************************************************************************************/

#ifndef CONFIG_H_
#define CONFIG_H_

/************* Includes *******************************************************************/


/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/
#define Auto

//#define CAR_V1
#define CAR_V2
//#define USE_RX24F
//#define WENC_V3 /* wheel encoder with tacho & direction output */

//#define Board

/* Modules used */

/* DEFINES for use of RC control for development */
//#define RC_CONTROL_EN

/* DEFINES for use of field oriented control */
//#define PMSM_FOC_EN


//#define SRF02_EN			// enable the SRF02 device
#define SRF02_T_EN			// enable the SRF02 device
#define I2C_EN				// enable the i2c module
#define ADC_EN
#define PWM_EN

//#define FOUR_WHEEL_ODOM

/*!
 * I2C
 */
#define I2C_CH1_EN
//#define I2C_CH2_EN
//#define I2C_CH3_EN


/*!
 * ADC Channels
 */
#define ADC_CH1_EN
#define ADC_CH2_EN
#define ADC_CH3_EN
#define ADC_CH4_EN
#define ADC_CH5_EN
#define ADC_CH6_EN
#define ADC_CH7_EN
#define ADC_CH8_EN
//#define ADC_CH9_EN

/*!
 * PWM Channels
 */
#define PWM_CH1_EN
#define PWM_CH2_EN
//#define PWM_CH3_EN
//#define PWM_CH4_EN

/************* Private static variables ***************************************************/


/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/


/************* Private functions **********************************************************/

#endif /* CONFIG_H_ */
