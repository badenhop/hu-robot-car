/********************************************************************************************
 * Unit in charge:
 * @file	paramsys.c
 * @author	freyer
 * $Date:: 2015-02-26 14:28:45 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1344                  $
 * $Author:: hepner             $
 * 
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 * This unit create the paramter groups an der address array for the groups. This unit also
 * include the functions for the access to the parametersystem.
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sys/paramsys/paramsys.h"
#include "sys/paramsys/paramlist.h"
#include "sys/util/io.h"


/************* Private typedefs ***********************************************************/

#define PARAMSYS_INIT_FAILED     1            /* The connection of the param pool failed */

/************* Macros and constants *******************************************************/



/************* Private static variables ***************************************************/

/* creating the arrays for the groups */
#ifdef paraGrp_01_EN
	static paramSysParam_t paramStruct01[sizeGrp01];
#endif

#ifdef paraGrp_02_EN
	static paramSysParam_t paramStruct02[sizeGrp02];
#endif

#ifdef paraGrp_03_EN
	static paramSysParam_t paramStruct03[sizeGrp03];
#endif

#ifdef paraGrp_04_EN
	static paramSysParam_t paramStruct04[sizeGrp04];
#endif

#ifdef paraGrp_05_EN
	static paramSysParam_t paramStruct05[sizeGrp05];
#endif

#ifdef paraGrp_06_EN
	static paramSysParam_t paramStruct06[sizeGrp06];
#endif

#ifdef paraGrp_07_EN
	static paramSysParam_t paramStruct07[sizeGrp07];
#endif

#ifdef paraGrp_08_EN
	static paramSysParam_t paramStruct08[sizeGrp08];
#endif

#ifdef paraGrp_09_EN
	static paramSysParam_t paramStruct09[sizeGrp09];
#endif

#ifdef paraGrp_10_EN
	static paramSysParam_t paramStruct10[sizeGrp10];
#endif

#ifdef paraGrp_11_EN
	static paramSysParam_t paramStruct11[sizeGrp11];
#endif

#ifdef paraGrp_12_EN
	static paramSysParam_t paramStruct12[sizeGrp12];
#endif

#ifdef paraGrp_13_EN
	static paramSysParam_t paramStruct13[sizeGrp13];
#endif

#ifdef paraGrp_14_EN
	static paramSysParam_t paramStruct14[sizeGrp14];
#endif

#ifdef paraGrp_15_EN
	static paramSysParam_t paramStruct15[sizeGrp15];
#endif

#ifdef paraGrp_16_EN
	static paramSysParam_t paramStruct16[sizeGrp16];
#endif

#ifdef paraGrp_17_EN
	static paramSysParam_t paramStruct17[sizeGrp17];
#endif

#ifdef paraGrp_18_EN
	static paramSysParam_t paramStruct18[sizeGrp18];
#endif

#ifdef paraGrp_19_EN
	static paramSysParam_t paramStruct19[sizeGrp19];
#endif

#ifdef paraGrp_20_EN
	static paramSysParam_t paramStruct20[sizeGrp20];
#endif

#ifdef paraGrp_21_EN
	static paramSysParam_t paramStruct21[sizeGrp21];
#endif

#ifdef paraGrp_22_EN
	static paramSysParam_t paramStruct22[sizeGrp22];
#endif

#ifdef paraGrp_23_EN
	static paramSysParam_t paramStruct23[sizeGrp23];
#endif

#ifdef paraGrp_24_EN
	static paramSysParam_t paramStruct24[sizeGrp24];
#endif

#ifdef paraGrp_25_EN
	static paramSysParam_t paramStruct25[sizeGrp25];
#endif

#ifdef paraGrp_26_EN
	static paramSysParam_t paramStruct26[sizeGrp26];
#endif

#ifdef paraGrp_27_EN
	static paramSysParam_t paramStruct27[sizeGrp27];
#endif

#ifdef paraGrp_28_EN
	static paramSysParam_t paramStruct28[sizeGrp28];
#endif

#ifdef paraGrp_29_EN
	static paramSysParam_t paramStruct29[sizeGrp29];
#endif

#ifdef paraGrp_30_EN
	static paramSysParam_t paramStruct30[sizeGrp30];
#endif

#ifdef paraGrp_31_EN
	static paramSysParam_t paramStruct31[sizeGrp31];
#endif

#ifdef paraGrp_32_EN
	static paramSysParam_t paramStruct32[sizeGrp32];
#endif

#ifdef paraGrp_33_EN
	static paramSysParam_t paramStruct33[sizeGrp33];
#endif

#ifdef paraGrp_34_EN
	static paramSysParam_t paramStruct34[sizeGrp34];
#endif

#ifdef paraGrp_35_EN
	static paramSysParam_t paramStruct35[sizeGrp35];
#endif

#ifdef paraGrp_36_EN
	static paramSysParam_t paramStruct36[sizeGrp36];
#endif

#ifdef paraGrp_37_EN
	static paramSysParam_t paramStruct37[sizeGrp37];
#endif

#ifdef paraGrp_38_EN
	static paramSysParam_t paramStruct38[sizeGrp38];
#endif

#ifdef paraGrp_39_EN
	static paramSysParam_t paramStruct39[sizeGrp39];
#endif

#ifdef paraGrp_40_EN
	static paramSysParam_t paramStruct40[sizeGrp40];
#endif



/* Array with the pointers for the start-addresses of the parameter groups */
/* The index 0 is unused, because of correspondence to the groups numbers. */
paramSysGrp_t grpStruct[numbOfGrps+1];


/************* Private function prototypes ************************************************/

/**
* @brief Initialization of the  parameter groups. Fill the elements (parameter) of the groups with data (attributes).
*
* @param [in,out] nothing.
*
* @return 0 = OK, anything else = error
*/
int16_t paramsys_initGrp(void);

/**
* @brief Initialization of the  parameter groups. Fill the elements (parameter) of the groups with data (attributes).
*
* @param paramSysGrp_t grpStructArray[], pointer to the grpStruct array (array with the start addresses of the group arrays).
*
* @return 0 = OK, anything else = error
*/
int16_t paramsys_setupGrp(paramSysGrp_t grpStructArray[]);

/**
* @brief This function include all the paramConnect calls. The call of this function connect all the pointer with
* 		 them parameters of the Parametersystem.
*
* @param [in,out] nothing.
*
* @return 0 = OK, anything else = error
*/
int8_t paramsys_initConnect(void);



/************* Public functions ***********************************************************/


void paramsys_init(void) {

	/* Create the parameter goups */
	if (paramsys_initGrp() != 0) {
		io_printf("\n*** Error: Initialization of the group fault\n");
	}
	/* Fill the parameter goups with data */
	if (paramsys_setupGrp(grpStruct) != 0) {
		io_printf("\n*** Error: Setup of the parameter-group fault\n");
	}
	/* Connect the pointer with the parameters */
	if (paramsys_initConnect() != 0) {
		io_printf("\n*** Error: Connect pointer with the parameters fault\n");
	}
}


int16_t paramsys_list(void) {

	int16_t error = 0;

	io_printf("\nAvailable Parameters are:");
	io_printf("\n=========================\n");

	/* loop for the groups */
	for (uint8_t i = 1; i < numbOfGrps+1; i++) {

		uint8_t numbElem = grpStruct[i].numbElem;
		io_printf("\nGroup: %s\n\n", grpStruct[i].nameGrp);

		/* loop for the elements of the groups */
		for (uint8_t j = 0; j < numbElem; j++) {

			io_printf("%04d", grpStruct[i].startAddress[j].numb);
			io_printf(" - %s\n", grpStruct[i].startAddress[j].name);
		}

		io_printf("_________________________\n");

	}

	return error;
}


int16_t paramsys_checkID(param_id_t param) {

	int16_t error;

	/* check for illegal parameter group or index */
	if ((param.grp > numbOfGrps) || (param.index > grpStruct[param.grp].numbElem) || (param.index == 0) || (param.grp == 0)) {
		error = 1;
	/*FRY... the check (param.grp > numbOfGrps) is not correct, because is there no grpStruct[param.grp].numbElem the check returns also a failure */
	}
	/* if everything is ok */
	else {
		error = 0;
	}
	return error;	/* 0 = OK,   1 = wrong ID */
}



int16_t paramsys_default(param_id_t param) {

	int16_t error;

	/* check for illegal parameter group or index and set error code */
	if (paramsys_checkID(param) != 0) {
		error = 1;
	}
	/* set the parameter to default */
	else {
		grpStruct[param.grp].startAddress[param.index-1].paramActValue = grpStruct[param.grp].startAddress[param.index-1].paramDefValue;
		error = 0;
	}
	return error;	// 0 = OK, anything else = error (details to be specified)
}


paramSysParam_t paramsys_paramRead(param_id_t param) {

	/* return a copy of the parameter structure */
	return grpStruct[param.grp].startAddress[param.index-1];
}


param_value_t paramsys_paramValue(param_id_t param) {

	param_value_t retVal;

	/* switch to parameter type an get the enum for the type and the value */
	switch (grpStruct[param.grp].startAddress[param.index-1].variType) {

		default:
			retVal.value.valueInt  = 0;
			retVal.type = PARAM_INT;
			break;

		case PARAM_INT:
			retVal.value.valueInt = grpStruct[param.grp].startAddress[param.index-1].paramActValue.valueInt;
			retVal.type = PARAM_INT;
			break;

		case PARAM_UINT:
			retVal.value.valueUint = grpStruct[param.grp].startAddress[param.index-1].paramActValue.valueUint;
			retVal.type = PARAM_UINT;
			break;

		case PARAM_FLOAT:
			retVal.value.valueFloat = grpStruct[param.grp].startAddress[param.index-1].paramActValue.valueFloat;
			retVal.type = PARAM_FLOAT;
			break;
	}
	return retVal;
}

int16_t paramsys_paramWrite(param_id_t param, param_value_t newValue) {

	int16_t error;

	/* check for illegal parameter group or index and set error code */
	if (paramsys_checkID(param) == 0) {

		/* check if the type of the new value the same as the act. value of the parameter */
		if ((grpStruct[param.grp].startAddress[param.index-1].variType) == (newValue.type)) {

			/* switch to parameter type */
			switch (grpStruct[param.grp].startAddress[param.index-1].variType) {

				case PARAM_INT:

					/* check range of new value --> min value of parameter */
					if (grpStruct[param.grp].startAddress[param.index-1].paramMinValue.valueInt > newValue.value.valueInt) {
						error = 2;
					}

					/* check range of new value --> max value of parameter */
					else if(grpStruct[param.grp].startAddress[param.index-1].paramMaxValue.valueInt < newValue.value.valueInt) {
						error = 3;
					}

					/* is everything ok, write integer Parameter */
					else {
						grpStruct[param.grp].startAddress[param.index-1].paramActValue.valueInt = newValue.value.valueInt;
						error = 0;
					}

					break;

				case PARAM_UINT:

					/* check range of new value --> min value of parameter */
					if (grpStruct[param.grp].startAddress[param.index-1].paramMinValue.valueUint > newValue.value.valueUint) {
						error = 2;
					}

					/* check range of new value --> max value of parameter */
					else if(grpStruct[param.grp].startAddress[param.index-1].paramMaxValue.valueUint < newValue.value.valueUint) {
						error = 3;
					}

					/* is everything ok, write unsigned integer  Parameter */
					else {
						grpStruct[param.grp].startAddress[param.index-1].paramActValue.valueUint = newValue.value.valueUint;
						error = 0;
					}

					break;

				case PARAM_FLOAT:

					/* check range of new value --> min value of parameter */
					if (grpStruct[param.grp].startAddress[param.index-1].paramMinValue.valueFloat > newValue.value.valueFloat) {
						error = 2;
					}

					/* check range of new value --> max value of parameter */
					else if(grpStruct[param.grp].startAddress[param.index-1].paramMaxValue.valueFloat < newValue.value.valueFloat) {
						error = 3;
					}

					/* is everything ok, write float Parameter */
					else {
						grpStruct[param.grp].startAddress[param.index-1].paramActValue.valueFloat = newValue.value.valueFloat;
						error = 0;
					}

					break;

				default:

					error = 4;

					break;
			}
		}
		else {

			error = 4;
		}

		//return error;	// 0 = OK, anything else = error ( 1 = wrong ID, 2 = value to small, 3 = value to big, 4 = other fault)
	}
	else {
		error = 1;
	}
	return error;	/* 0 = OK, anything else = error ( 1 = wrong ID, 2 = value to small, 3 = value to big, 4 = other fault) */
}


/************* Private functions **********************************************************/


int16_t paramsys_initGrp(void) {

	int16_t error = 0;

	/* fill the address array with blanco data for the address of the groups */
	for (uint8_t i = 0; i < numbOfGrps+1; i++) {

		grpStruct[i].startAddress = (paramSysParam_t*)NULL;

	}

/* fill the address array with the information data of the groups */
#ifdef paraGrp_01_EN
	grpStruct[1].startAddress = &paramStruct01[0];
	grpStruct[1].numbElem = sizeGrp01;
	grpStruct[1].numbGrp = 1;
	strcpy(grpStruct[1].nameGrp, "HW Setup");
#endif

#ifdef paraGrp_02_EN
	grpStruct[2].startAddress = &paramStruct02[0];
	grpStruct[2].numbElem = sizeGrp02;
	grpStruct[2].numbGrp = 2;
	strcpy(grpStruct[2].nameGrp, "SW Setup");
#endif

#ifdef paraGrp_03_EN
	grpStruct[3].startAddress = &paramStruct03[0];
	grpStruct[3].numbElem = sizeGrp03;
	grpStruct[3].numbGrp = 3;
	strcpy(grpStruct[2].nameGrp, "Operation Data");
#endif

#ifdef paraGrp_04_EN
	grpStruct[4].startAddress = &paramStruct04[0];
	grpStruct[4].numbElem =sizeGrp04;
	grpStruct[4].numbGrp = 4;
	strcpy(grpStruct[4].nameGrp, "Int Sensor Calib");
#endif

#ifdef paraGrp_05_EN
	grpStruct[5].startAddress = &paramStruct05[0];
	grpStruct[5].numbElem = sizeGrp05;
	grpStruct[5].numbGrp = 5;
	strcpy(grpStruct[5].nameGrp, "_");
#endif

#ifdef paraGrp_06_EN
	grpStruct[6].startAddress = &paramStruct06[0];
	grpStruct[6].numbElem = sizeGrp06;
	grpStruct[6].numbGrp = 6;
	strcpy(grpStruct[6].nameGrp, "_");
#endif

#ifdef paraGrp_07_EN
	grpStruct[7].startAddress = &paramStruct07[0];
	grpStruct[7].numbElem = sizeGrp07;
	grpStruct[7].numbGrp = 7;
	strcpy(grpStruct[7].nameGrp, "_");
#endif

#ifdef paraGrp_08_EN
	grpStruct[8].startAddress = &paramStruct08[0];
	grpStruct[8].numbElem = sizeGrp08;
	grpStruct[8].numbGrp = 8;
	strcpy(grpStruct[8].nameGrp, "_");
#endif

#ifdef paraGrp_09_EN
	grpStruct[9].startAddress = &paramStruct09[0];
	grpStruct[9].numbElem =sizeGrp09;
	grpStruct[9].numbGrp = 9;
	strcpy(grpStruct[9].nameGrp, "_");
#endif

#ifdef paraGrp_10_EN
	grpStruct[10].startAddress = &paramStruct10[0];
	grpStruct[10].numbElem = sizeGrp10;
	grpStruct[10].numbGrp = 10;
	strcpy(grpStruct[10].nameGrp, "Motor");
#endif

#ifdef paraGrp_11_EN
	grpStruct[11].startAddress = &paramStruct11[0];
	grpStruct[11].numbElem = sizeGrp11;
	grpStruct[11].numbGrp = 11;
	strcpy(grpStruct[11].nameGrp, "Current Sensors");
#endif

#ifdef paraGrp_12_EN
	grpStruct[12].startAddress = &paramStruct12[0];
	grpStruct[12].numbElem = sizeGrp12;
	grpStruct[12].numbGrp = 12;
	strcpy(grpStruct[12].nameGrp, "_");
#endif

#ifdef paraGrp_13_EN
	grpStruct[13].startAddress = &paramStruct13[0];
	grpStruct[13].numbElem = sizeGrp13;
	grpStruct[13].numbGrp = 13;
	strcpy(grpStruct[13].nameGrp, "_");
#endif

#ifdef paraGrp_14_EN
	grpStruct[14].startAddress = &paramStruct14[0];
	grpStruct[14].numbElem =sizeGrp14;
	grpStruct[14].numbGrp = 14;
	strcpy(grpStruct[14].nameGrp, "_");
#endif

#ifdef paraGrp_15_EN
	grpStruct[15].startAddress = &paramStruct15[0];
	grpStruct[15].numbElem = sizeGrp15;
	grpStruct[15].numbGrp = 15;
	strcpy(grpStruct[15].nameGrp, "_");
#endif

#ifdef paraGrp_16_EN
	grpStruct[16].startAddress = &paramStruct16[0];
	grpStruct[16].numbElem = sizeGrp16;
	grpStruct[16].numbGrp = 16;
	strcpy(grpStruct[16].nameGrp, "_");
#endif

#ifdef paraGrp_17_EN
	grpStruct[17].startAddress = &paramStruct17[0];
	grpStruct[17].numbElem = sizeGrp17;
	grpStruct[17].numbGrp = 17;
	strcpy(grpStruct[17].nameGrp, "_");
#endif

#ifdef paraGrp_18_EN
	grpStruct[18].startAddress = &paramStruct18[0];
	grpStruct[18].numbElem = sizeGrp18;
	grpStruct[18].numbGrp = 18;
	strcpy(grpStruct[18].nameGrp, "_");
#endif

#ifdef paraGrp_19_EN
	grpStruct[19].startAddress = &paramStruct19[0];
	grpStruct[19].numbElem =sizeGrp19;
	grpStruct[19].numbGrp = 19;
	strcpy(grpStruct[19].nameGrp, "_");
#endif

#ifdef paraGrp_20_EN
	grpStruct[20].startAddress = &paramStruct20[0];
	grpStruct[20].numbElem = sizeGrp20;
	grpStruct[20].numbGrp = 20;
	strcpy(grpStruct[20].nameGrp, "Veh Dimensions");
#endif

#ifdef paraGrp_21_EN
	grpStruct[21].startAddress = &paramStruct21[0];
	grpStruct[21].numbElem = sizeGrp21;
	grpStruct[21].numbGrp = 21;
	strcpy(grpStruct[21].nameGrp, "Veh Dynamics");
#endif

#ifdef paraGrp_22_EN
	grpStruct[22].startAddress = &paramStruct22[0];
	grpStruct[22].numbElem = sizeGrp22;
	grpStruct[22].numbGrp = 22;
	strcpy(grpStruct[22].nameGrp, "Veh Control");
#endif

#ifdef paraGrp_23_EN
	grpStruct[23].startAddress = &paramStruct23[0];
	grpStruct[23].numbElem = sizeGrp23;
	grpStruct[23].numbGrp = 23;
	strcpy(grpStruct[23].nameGrp, "_");
#endif

#ifdef paraGrp_24_EN
	grpStruct[24].startAddress = &paramStruct24[0];
	grpStruct[24].numbElem =sizeGrp24;
	grpStruct[24].numbGrp = 24;
	strcpy(grpStruct[24].nameGrp, "_");
#endif

#ifdef paraGrp_25_EN
	grpStruct[25].startAddress = &paramStruct25[0];
	grpStruct[25].numbElem = sizeGrp25;
	grpStruct[25].numbGrp = 25;
	strcpy(grpStruct[25].nameGrp, "_");
#endif

#ifdef paraGrp_26_EN
	grpStruct[26].startAddress = &paramStruct26[0];
	grpStruct[26].numbElem = sizeGrp26;
	grpStruct[26].numbGrp = 26;
	strcpy(grpStruct[26].nameGrp, "_");
#endif

#ifdef paraGrp_27_EN
	grpStruct[27].startAddress = &paramStruct27[0];
	grpStruct[27].numbElem = sizeGrp27;
	grpStruct[27].numbGrp = 27;
	strcpy(grpStruct[27].nameGrp, "_");
#endif

#ifdef paraGrp_28_EN
	grpStruct[28].startAddress = &paramStruct28[0];
	grpStruct[28].numbElem = sizeGrp28;
	grpStruct[28].numbGrp = 28;
	strcpy(grpStruct[28].nameGrp, "_");
#endif

#ifdef paraGrp_29_EN
	grpStruct[29].startAddress = &paramStruct29[0];
	grpStruct[29].numbElem =sizeGrp29;
	grpStruct[29].numbGrp = 29;
	strcpy(grpStruct[29].nameGrp, "_");
#endif

#ifdef paraGrp_30_EN
	grpStruct[30].startAddress = &paramStruct30[0];
	grpStruct[30].numbElem = sizeGrp30;
	grpStruct[30].numbGrp = 30;
	strcpy(grpStruct[30].nameGrp, "_");
#endif

#ifdef paraGrp_31_EN
	grpStruct[31].startAddress = &paramStruct31[0];
	grpStruct[31].numbElem = sizeGrp31;
	grpStruct[31].numbGrp = 31;
	strcpy(grpStruct[31].nameGrp, "_");
#endif

#ifdef paraGrp_32_EN
	grpStruct[32].startAddress = &paramStruct32[0];
	grpStruct[32].numbElem = sizeGrp32;
	grpStruct[32].numbGrp = 32;
	strcpy(grpStruct[32].nameGrp, "_");
#endif

#ifdef paraGrp_33_EN
	grpStruct[33].startAddress = &paramStruct33[0];
	grpStruct[33].numbElem = sizeGrp33;
	grpStruct[33].numbGrp = 33;
	strcpy(grpStruct[33].nameGrp, "_");
#endif

#ifdef paraGrp_34_EN
	grpStruct[34].startAddress = &paramStruct34[0];
	grpStruct[34].numbElem =sizeGrp34;
	grpStruct[34].numbGrp = 34;
	strcpy(grpStruct[34].nameGrp, "_");
#endif

#ifdef paraGrp_35_EN
	grpStruct[35].startAddress = &paramStruct35[0];
	grpStruct[35].numbElem = sizeGrp35;
	grpStruct[35].numbGrp = 35;
	strcpy(grpStruct[35].nameGrp, "_");
#endif

#ifdef paraGrp_36_EN
	grpStruct[36].startAddress = &paramStruct36[0];
	grpStruct[36].numbElem = sizeGrp36;
	grpStruct[36].numbGrp = 36;
	strcpy(grpStruct[36].nameGrp, "_");
#endif

#ifdef paraGrp_37_EN
	grpStruct[37].startAddress = &paramStruct37[0];
	grpStruct[37].numbElem = sizeGrp37;
	grpStruct[37].numbGrp = 37;
	strcpy(grpStruct[37].nameGrp, "_");
#endif

#ifdef paraGrp_38_EN
	grpStruct[38].startAddress = &paramStruct38[0];
	grpStruct[38].numbElem = sizeGrp38;
	grpStruct[38].numbGrp = 38;
	strcpy(grpStruct[38].nameGrp, "_");
#endif

#ifdef paraGrp_39_EN
	grpStruct[39].startAddress = &paramStruct39[0];
	grpStruct[39].numbElem =sizeGrp39;
	grpStruct[39].numbGrp = 39;
	strcpy(grpStruct[39].nameGrp, "_");
#endif

#ifdef paraGrp_40_EN
	grpStruct[40].startAddress = &paramStruct40[0];
	grpStruct[40].numbElem = sizeGrp40;
	grpStruct[40].numbGrp = 40;
	strcpy(grpStruct[40].nameGrp, "_");
#endif

	return error; /* 0 = OK, anything else = error (details to be specified) */
}


