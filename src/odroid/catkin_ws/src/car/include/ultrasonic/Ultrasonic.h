#ifndef ULTRASONIC_H
#define ULTRASONIC_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include "UltrasonicSensor.h"
#include "NetworkingLib/Networking.h"

namespace car
{

class Ultrasonic : public nodelet::Nodelet
{
public:
    Ultrasonic();

    void onInit() override;

private:
    ros::NodeHandle nh;
	ros::Publisher ussDistancePublisher;
    UltrasonicSensor::Ptr sensor;
    networking::Networking net;

    static int readDevId();
};

}
#endif
