#!/bin/bash

# set link for config files if not already existing
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DEST=$DIR/CarConfig
LINK=$HOME/CarConfig
if [ ! -e $LINK ]; then
  echo "ln -s $DEST $HOME"
  ln -s $DEST $HOME
fi

# install NetworkingLib
cd catkin_ws/src/NetworkingLib/
mkdir cmake-build-debug
cd cmake-build-debug
cmake ..
make -j7
make install

# build VeloxProtocolLib
cd ../../VeloxProtocolLib/
mkdir cmake-build-debug
cd cmake-build-debug
cmake ..
make -j7
make install

# build ROS stuff
cd ../../../
catkin_make
