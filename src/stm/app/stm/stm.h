/** *****************************************************************************************
 * Unit in charge:
 * @file	stm.h
 * @author	Brieske
 * $Date:: 2017-06-15 13:17:03 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1744                  $
 * $Author:: mbrieske   $
 *
 * ----------------------------------------------------------
 *
 * @brief This stamemachine is the system's central management unit. It manages the control type and
 * it monitors the start-up precedure and releases the PWM to the power electronics in case of a
 * successful start-up.
 *
 ******************************************************************************************/


#ifndef STM_H_
#define STM_H_

#include "per/irq.h"
#include "app/mavlink/mavlink.h"
#include "dev/servo/servo.h"

/************* Includes *******************************************************************/

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/

int8_t stm_init(void);

void stm_step(void);

#endif /* STM_H_ */
