/** *****************************************************************************************
 * Unit in charge:
 * @file	ramp.c
 * @author	Yoga
 * $Date:: 2016-05-23 14:03:20 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 0                  $
 * $Author:: Yoga          $
 *
 * ----------------------------------------------------------
 *
 * @brief This function generates the a of a traget speed
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "sys/algo/ramp.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/


/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
void ramp_init(ramp_t* pRamp)
{

	/* initialize the variable */
	pRamp->spdRampOld = 0.0;
	
	return;
}

void ramp_step(ramp_t* pRamp, float32_t spdTgt, float32_t* spdRampAct)
{

	/* determine  the actual speed ramp value */
	if(spdTgt > pRamp->spdRampOld){
		*spdRampAct = pRamp->spdRampOld + pRamp->increaseSpd;
	}
	else if(spdTgt < pRamp->spdRampOld){
		*spdRampAct = pRamp->spdRampOld - pRamp->brakeSpd;
	}
	else{
		*spdRampAct = spdTgt;
	}
	
	if(fabsf(*spdRampAct) < pRamp->brakeSpd){
		*spdRampAct = 0.0;
	}

	/* write the actual speed ramp value for the next step */
	pRamp->spdRampOld = *spdRampAct;
}

/************* Private functions **********************************************************/
