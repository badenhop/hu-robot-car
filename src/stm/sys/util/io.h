/** *****************************************************************************************
 * Unit in charge:
 * @file	io.h
 * @author	Bjoern Hepner
 * $Date:: 2015-02-24 11:03:01 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1336                  $
 * $Author:: hepner             $
 * 
 * ----------------------------------------------------------
 *
 * @brief Sets an alias for printf and scanf for formatted output and input ready.
 *
 ******************************************************************************************/

#ifndef IO_H_
#define IO_H_

/************* Includes *******************************************************************/
#include "per/uart.h"
#include "ringbuf.h"

/************* Public typedefs ************************************************************/


/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the UART4 interface for IO functions with the baud rate 9600.
 *
 * @param [in,out] *uart structure from uart
 * @param [in] *pRecBuf	- Buffer used to buffer incoming communication of the uart.
 * @param [in] recBufSize - The size of the buffer for incoming communication.
 * @param [in] *pSendBuf	- Buffer used to buffer outgoing communication of the uart.
 * @param [in] sendBufSize - The size of the buffer for outgoing communication.
 * @param *pSendBufTimestamp - Timestamp in the transmit buffer
 * @param *pRecBufTimestamp -  Timestamp in the receiver buffer
 *
 * @return - 0 all right. Everything else is an error.
 */
int8_t io_init(uart_t* uart, int8_t* pRecBuf, uint32_t recBufSize, int8_t* pSendBuf, uint32_t sendBufSize, uint32_t *pSendBufTimestamp, uint32_t *pRecBufTimestamp);

/**
 * @brief Loads data from the given locations and writes them to the
 *        standard output according to the format parameter.
 *
 *        For a formatted output following types are available.
 *        The argument contains a format string that may include
 *        conversion specifications. Each conversion specification
 *        is introduced by the character %, and ends with a
 *        conversion specifier.
 *
 *        The following conversion specifiers are supported
 *        cdisuxX%
 *
 *        Usage:
 *        c    character
 *        d,i  signed integer (-sign added, + sign not supported)
 *        s    character string
 *        u    unsigned integer as decimal
 *        x,X  unsigned integer as hexadecimal (uppercase letter)
 *        %    % is written (conversion specification is '%%')
 *        f    float
 *
 *
 * @param [in,out] *pFmt Data for output
 *
 * @return Number of bytes written
 *
 */
int8_t io_printf(const char_t* pFmt, ...);

/**
 * @brief     For a formatted input following types are available.
 *
 *				The following conversion specifiers are supported
 *				cdisuxX%
 *
 *				Usage:
 *				c			character
 *				d,i,u		integer
 *				s			character string
 *				x,X			unsigned integer as hexadecimal (uppercase letter)
 *				f,e,g		float
 *
 *
 * @param [in,out] *pFmt Data for output
 *
 * @return 0 = Everything is okay.
 * @return IO_SCANF_INPUT_SIZE_FULL
 * @return IO_SCANF_WRONG_FORMAT_SPECIFIER
 *
 */
int8_t io_scanf(const char_t* pFmt, ...);

/**
 * @brief Function recognizes if a character was entered. In this case, the current value will be returned. Otherwise the return value will be 0.
 *
 * n can assume the following values:
 *
 * 		1 = Byte has been read.
 * 		0 = There is nothing to read.
 * 	   -1 = Error Message
 *
 * @return Read-in character or error message.
 *
 */
int8_t io_getchar(void);

/**
 * @brief Reads an input in the terminal as a complete string. The entry must be confirmed with Enter.
 *
 * @param *pBuf contains the line read.
 * @param size the length of the line.
 *
 * @return Number of characters read.
 * @return IO_GETLINE_INPUT_SIZE_FULL
 */
int8_t io_getline(int8_t *pBuf, int8_t size);

/**
 * @brief Reads an input in the terminal as a complete string. The function is not complete. If after a long string is a short, is the long not completely deleted.
 *
 * @param *pResolution - Variable contains the line read.
 *
 * @return 0
 */
int8_t io_getlineTerminal(int8_t *pResolution);


#endif /* IO_H_ */

