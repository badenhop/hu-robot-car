/** *****************************************************************************************
 * Unit in charge:
 * @file	ramp.h
 * @author	Yoga
 * $Date:: 2016-05-13 14:28:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 0                  $
 * $Author:: Yoga             $
 *
 * ----------------------------------------------------------
 *
 * @brief This function generates the a of a traget speed
 *
 ******************************************************************************************/


#ifndef RAMP_H_
#define RAMP_H_

/************* Includes *******************************************************************/
#include <math.h>
#include "brd/startup/stm32f4xx.h"

/************* Public typedefs ************************************************************/
typedef struct
{
	float32_t spdRampOld;
	float32_t increaseSpd;    /* describe the steepness  of the lines */
	float32_t brakeSpd;
} ramp_t;

/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/

/**
 * @brief	Initializes the function ramp_step.
 *
 * @param[out] ramp_t* pRamp is the pointer to the ramp_t struct containing the steepness "slope" of the ramp of manipulated variable spdRamp.
 *
 * @return void
 */
void ramp_init(ramp_t* pRamp);

/**
 * @brief	Function for calculation of manipulated variable spdRamp.
 *
 * @param[in,out] ramp_t* pRamp is a pointer to the ramp_t struct containing the steepness "slope" of the ramp of manipulated variable spdRamp.
 *
 * @param[out] float32_t spdRamp is the actuating variable
 * @param[in] float32_t  spdTgt is the setpoint target speed
 * @param[in] float32_t slope describe the steepness  of the lines
 *
 * @return void
 */
void ramp_step(ramp_t* p_ramp, float32_t spdTgt, float32_t* spdRampAct);

#endif /* RAMP_H_ */
