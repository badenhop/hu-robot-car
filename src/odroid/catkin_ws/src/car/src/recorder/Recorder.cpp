//
// Created by philipp on 20.06.18.
//

#include <pluginlib/class_list_macros.h>
#include <thread>
#include "recorder/Recorder.h"
#include <boost/filesystem.hpp>
#include <utils/Config.h>

PLUGINLIB_EXPORT_CLASS(car::Recorder, nodelet::Nodelet);

namespace car
{

Recorder::Recorder()
	: capture(744, 480, 60)
{}

void Recorder::onInit()
{
	wheelTicksSubscriber = nh.subscribe("WheelTicks", 1000, &Recorder::wheelTicksCallback, this);
	setThrottleSubscriber = nh.subscribe("SetThrottle", 1, &Recorder::setThrottleCallback, this);
	setAngleSubscriber = nh.subscribe("SetAngle", 1, &Recorder::setAngleCallback, this);
	ussDistanceSubscriber = nh.subscribe("UssDistance", 1, &Recorder::ussDistanceCallback, this);
	stopRecordingSubscriber = nh.subscribe("StopRecording", 1, &Recorder::stopRecordingCallback, this);
	remoteStateSubscriber = nh.subscribe("RemoteState", 1, &Recorder::remoteStateCallback, this);

	readConfig();

	capture.start(
		[&](auto & frame)
		{
			std::lock_guard<std::mutex> lock{frameCopyMutex};
			currFrame = frame.clone();
			receivedFrame = true;
		});

	std::thread recorderThread{[&] { this->record(); }};
	recorderThread.detach();
}

void Recorder::wheelTicksCallback(const WheelTicks::ConstPtr msg)
{
	std::lock_guard<std::mutex> lock{ticksMutex};
	ticksRL += msg->rearLeftTicks;
	ticksRR += msg->rearRightTicks;
}

void Recorder::setThrottleCallback(const SetThrottle::ConstPtr msg)
{
	setThrottle = msg->throttle;
}

void Recorder::setAngleCallback(const SetAngle::ConstPtr msg)
{
	setAngle = msg->angle;
}

void Recorder::ussDistanceCallback(const UssDistance::ConstPtr msg)
{
	distance = msg->distance;
}

void Recorder::stopRecordingCallback(const StopRecording::ConstPtr msg)
{
	recording = false;
}

void Recorder::remoteStateCallback(const RemoteState::ConstPtr msg)
{
	if (msg->id == 0)
		recordingSequence = msg->value != 0 ? true : false;
}

void Recorder::record()
{
	auto dataDirName = createRecordingDirectory();

	std::ostringstream oss;
	oss << dataDirName << "/data.csv";
	std::ofstream dataFile{oss.str()};

	dataFile << "ticksRL,ticksRR,setThrottle,setAngle,distance,recording,img\n";

	auto last = networking::time::now();

	for (std::size_t counter = 0; recording; counter++)
	{
		// keep constant frame rate
		auto now = networking::time::now();
		auto lastFrameTime = now - last;
		if (lastFrameTime < frameTime)
			std::this_thread::sleep_for(frameTime - lastFrameTime);
		last = networking::time::now();

		while (!receivedFrame);
		cv::Mat frame{};
		{
			std::lock_guard<std::mutex> lock{frameCopyMutex};
			frame = currFrame;
		}
		if (frame.empty())
			continue;

		// save image
		oss = std::ostringstream{};
		oss << "img-" << counter << ".jpg";
		auto imgFilename = oss.str();
		oss = std::ostringstream{};
		oss << dataDirName << "/" << imgFilename;
		auto imgPath = oss.str();
		cv::imwrite(imgPath, frame);

		writeCsvRecord(dataFile, imgFilename);
	}
}

std::string Recorder::createRecordingDirectory()
{
	std::ostringstream oss;
	auto time = std::time(nullptr);
	oss << recordingsDirectory << "/data-" << std::put_time(std::localtime(&time), "%Y-%m-%d-%H-%M-%S");
	auto dirName = oss.str();
	boost::filesystem::path dir{dirName};
	boost::filesystem::create_directory(dir);
	return dirName;
}

void Recorder::writeCsvRecord(std::ofstream & dataFile, const std::string & imgFilename)
{
	float ticksRLCopy{0}, ticksRRCopy{0};
	{
		std::lock_guard<std::mutex> lock{ticksMutex};
		ticksRLCopy = ticksRL;
		ticksRRCopy = ticksRR;
		ticksRL = 0;
		ticksRR = 0;
	}

	dataFile << ticksRLCopy << ","
			 << ticksRRCopy << ","
	         << setThrottle.load() << ","
	         << setAngle.load() << ","
	         << distance.load() << ","
			 << recordingSequence.load() << ","
	         << imgFilename << "\n";
}

void Recorder::readConfig()
{
	auto j = config::open();

	if (j.count("recordingsDirectory") > 0)
		recordingsDirectory = j.at("recordingsDirectory").get<std::string>();
	else
		recordingsDirectory = config::directory() + "/recordings";

	int fps = 60;
	if (j.count("recordingFPS") > 0)
		fps = j.at("recordingFPS").get<int>();
	frameTime = std::chrono::nanoseconds(1000000000 / fps);
}

}