/** *****************************************************************************************
 * Unit in charge:
 * @file	srf02.h
 * @author	ledwig
 * $Date:: 2014-05-16 13:48:47 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 473                   $
 * $Author:: wandji-kwuntchou   $
 *
 * ----------------------------------------------------------
 *
 * @brief A supersonic device with i2c attachment
 *
 ******************************************************************************************/

#ifndef SRF02_H_
#define SRF02_H_

/************* Includes *******************************************************************/
#include "per/i2c.h"

#ifdef SRF02_EN

/************* Public typedefs ************************************************************/
typedef struct
{
	uint8_t address; /* address of the I2C slave */
	i2c_channel_t channel;
} srf02_t;

/************* Macros and constants *******************************************************/
#define SRF02_OK 	0
#define SRF02_ERR 	-1

/************* Public function prototypes *************************************************/

/**
 * @brief	Initializes the chosen SRF02 device
 *
 * @param[in,out] device contains the SRF02 configuration type such as the device address and i2c channel to initialize
 * @param[in,out] i2c contains a i2c configuration type to initialize
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02_init(srf02_t* pDevice, i2c_t* pI2c);

/**
 * @brief	changes the address of the initialized SRF02 device
 *
 * @param[in,out] device contains the SRF02 configuration type
 * @param[in]     address is the new address for SRF02 device
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02_setAddress(srf02_t* pDevice, uint8_t address);

/**
 * @brief	returns revision value of SRF02 device by reading from instruction register
 *
 * @param[in,out] device contains the SRF02 configuration type
 * @param[in,out] pData contains the revision value
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02_getRevision(srf02_t* pDevice, uint8_t* pData);

/**
 * @brief	returns the minimum range value of SRF02 device by reading from register 4 and 5
 *
 * @param[in,out] device contains the SRF02 configuration type
 * @param[in,out] pData contains the minimum range value in cm
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02_getRangeMinimum(srf02_t* pDevice, uint16_t* pData);

/**
 * @brief	returns the current measurement value of SRF02 device by reading from register 2 and 3
 *
 * @param[in,out] device contains the SRF02 configuration type
 * @param[in,out] pData contains the current measurement value in cm
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02_getMeasurement(srf02_t* pDevice, uint16_t* pData);

#endif /* SRF02_EN */
#endif /* SRF02_H_ */
