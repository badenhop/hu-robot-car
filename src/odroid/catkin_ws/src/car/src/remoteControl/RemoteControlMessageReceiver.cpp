//
// Created by philipp on 23.08.18.
//

#include <pluginlib/class_list_macros.h>
#include "remoteControl/RemoteControlMessageReceiver.h"
#include <boost/algorithm/string.hpp>

#include "car/RemoteThrottle.h"
#include "car/RemoteAngle.h"
#include "car/RemoteState.h"

PLUGINLIB_EXPORT_CLASS(car::RemoteControlMessageReceiver, nodelet::Nodelet);

namespace car
{

void RemoteControlMessageReceiver::onInit()
{
	remoteThrottlePublisher = nh.advertise<RemoteThrottle>("RemoteThrottle", 1);
	remoteAnglePublisher = nh.advertise<RemoteAngle>("RemoteAngle", 1);
	remoteStatePublisher = nh.advertise<RemoteState>("RemoteState", 1);

	messageReceiver = networking::message::DatagramReceiver<RemoteControlMessage>::create(net, 10288);
	receiveMessages();
}

void RemoteControlMessageReceiver::receiveMessages()
{
	messageReceiver->asyncReceive(
		networking::time::Duration::max(),
		[&](const auto & error, const auto & message, const auto & host, auto port)
		{
			if (error)
				return;
			this->handleMessage(message);
			this->receiveMessages();
		});
}

void RemoteControlMessageReceiver::handleMessage(const RemoteControlMessage & message)
{
	if (message.key == "angle")
	{
		RemoteAngle angleMsg;
		angleMsg.angle = std::stof(message.value);
		remoteAnglePublisher.publish(angleMsg);
	}
	else if (message.key == "speed")
	{
		RemoteThrottle throttleMsg;
		throttleMsg.throttle = std::stof(message.value);
		remoteThrottlePublisher.publish(throttleMsg);
	}
	else if (message.key == "state")
	{
		RemoteState remoteState;
		std::vector<std::string> split;
		boost::split(split, message.value, [](char c) { return c == ','; });
		remoteState.id = std::stoi(split[0]);
		remoteState.value = std::stoi(split[1]);
		remoteStatePublisher.publish(remoteState);
	}
}

}