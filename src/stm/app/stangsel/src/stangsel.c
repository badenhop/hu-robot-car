/** *****************************************************************************************
 * Unit in charge:
 * @file	stangsel.c
 * @author	hinze
 * $Date:: 2017-12-05 12:29:39 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske           $
 * 
 * ----------------------------------------------------------
 *
 * @brief Select steering angle depending on statemachine state
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "app/stangsel/stangsel.h"
#include "app/leddebug.h"

/************* Global variables ***********************************************************/
/* Pointers for input signals */
uint8_t *pState_stangsel;
carcontrol_t *pCarControl_stangsel;

/* Pointers for output signals */
float32_t *pStAngTgtSel_stangsel; /** */
bool_t* pRcOverrideStangEnabled_stangsel;

/************* Private typedefs ***********************************************************/

/************* Macros and constants *******************************************************/
#define SEL_STEER_ANGLE_MAX   29.0F
#define SEL_STEER_ANGLE_MIN   -29.0F

/************* Private static variables ***************************************************/
/* Static variable for the steering angle selector */
static float32_t selStAng;		   /* Private variable to store the steering angle output */
static bool_t rcOverride;

/************* Private function prototypes ************************************************/

/************* Public functions ***********************************************************/

int8_t stangsel_init(void)
{
	/* set output signal to init values */
	*pRcOverrideStangEnabled_stangsel = FALSE;
	*pStAngTgtSel_stangsel = 0.0;

	/* The init value of the steering angle storage */
	selStAng = 0.0F;

	return 0;
}

void stangsel_step(void)
{
	rcOverride = FALSE;

	irq_disable();
	uint8_t state = *pState_stangsel;
	irq_enable();

	switch (state)
	{
	case SYSTEM_STATE_INITIALIZING:
	case SYSTEM_STATE_IDLE:
	case SYSTEM_STATE_EMERGENCY:
		selStAng = 0;
		break;
	case SYSTEM_STATE_RUNNING_EXT:
		irq_disable();
		selStAng = pCarControl_stangsel->stAngTgtExt;
		irq_enable();
		break;
	case SYSTEM_STATE_RUNNING_RC:
		rcOverride = TRUE;
		break;
	default:
		// cannot go here
		break;
	}

	if(selStAng > SEL_STEER_ANGLE_MAX){
		selStAng = SEL_STEER_ANGLE_MAX;
	}
	if(selStAng < SEL_STEER_ANGLE_MIN){
		selStAng = SEL_STEER_ANGLE_MIN;
	}

	/* update output signals */
	irq_disable();
	*pRcOverrideStangEnabled_stangsel = rcOverride;
	*pStAngTgtSel_stangsel = selStAng;
	irq_enable();
}


/************* Private functions **********************************************************/
