/********************************************************************************************
 * Unit in charge:
 * @file    odom.c
 * @author  Neumerkel
 * $Date:: 2017-10-23 17:51:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2169              $
 * $Author:: 			$
 *
 * ----------------------------------------------------------
 * @brief The odometry module calculates the vehicles instantaneous x and y
 * coordinates relative to its initial starting point and the actual vehicle speed out of
 * incremental wheel encoder ticks.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../odom.h"
#include "app/config.h"
#include "sys/util/io.h"
#include "sys/systime/systime.h"

/************* Private typedefs ***********************************************************/
typedef struct
{
	float32_t yawAngle;
	float32_t xM;
	float32_t yM;
	float32_t speed;
	uint32_t timestamp;
} CarOdometry;
/************* Macros and constants *******************************************************/

#define PI					3.1415927F
#define US_TO_S         	0.000001F
#define MM_TO_M         	0.001F
#define MM_TO_CM      		0.1F

#define TRACK_WIDTH_M       0.261F
#define TRACK_WIDTH_MM      261.0F
#define DISC_RESOLUTION		120.0F
#define WHEEL_DIAMETER		0.11F

/************* Global variables ***********************************************************/

// input signals
int32_t *pWhlTicksFL_odom;
int32_t *pWhlTicksFR_odom;
int32_t *pWhlTicksRL_odom;
int32_t *pWhlTicksRR_odom;
int8_t *pCarDirection_odom;
uint16_t *pRcChannel1_odom;
bool_t *pRcOverrideSpdEnabled_odom;

// output signals
uint32_t *pOdomTimestamp_odom;
float32_t *pVehXDistOdom_odom;
float32_t *pVehYDistOdom_odom;
float32_t *pVehYawAngOdom_odom;
float32_t *pVehSpdOdom_odom;
int8_t *pVehDirOdom_odom;

/************* Private static variables ***************************************************/

static CarOdometry odometry;
static float32_t deltaMmLeftEncoderStep = 0.00;
static float32_t deltaMmRightEncoderStep = 0.00;

/************* Public functions **********************************************************/

int8_t odom_init(void)
{
	*pOdomTimestamp_odom = 0;
	odometry.yawAngle = 0.00;
	odometry.xM = 0.00;
	odometry.yM = 0.00;
	odometry.speed = 0.00;
	odometry.timestamp = 0;

	*pVehXDistOdom_odom = 0.0F;
	*pVehYDistOdom_odom = 0.0F;
	*pVehYawAngOdom_odom = 0.0F;
	*pVehSpdOdom_odom = 0.0F;
	*pVehDirOdom_odom = 0;

	// calculate delta s for 1 encoder impulse
	deltaMmLeftEncoderStep = (PI * WHEEL_DIAMETER / DISC_RESOLUTION) * 1000.0f;
	deltaMmRightEncoderStep = (PI * WHEEL_DIAMETER / DISC_RESOLUTION) * 1000.0f;

	return 0;
}

void odom_step(void)
{
	// local vars
	uint16_t ticksFL = 0;
	uint16_t ticksFR = 0;
	uint16_t ticksRL = 0;
	uint16_t ticksRR = 0;
	uint32_t actualTime = 0;
	float32_t sampleTime = 0.00;
	float32_t cosOfYawAngle = 0.00;
	float32_t sinOfYawAngle = 0.00;
	float32_t deltaYawAngle = 0.00;
#ifdef FOUR_WHEEL_ODOM
	float32_t deltaMmFL = 0;
	float32_t deltaMmFR = 0;
#endif
	float32_t deltaMmRL = 0;
	float32_t deltaMmRR = 0;
	float32_t deltaMmCar = 0;

	// get counter value for each encoder channel
	eict_getCounter(&ticksFL, &ticksFR, &ticksRL, &ticksRR);

	actualTime = systime_getSysTime();
	// calculate actual sample time
	sampleTime = (actualTime - odometry.timestamp) * US_TO_S;

	// (new ticks - old ticks) * deltaMmEncoderStep
	deltaMmRL = ticksRL * deltaMmLeftEncoderStep;
	deltaMmRR = ticksRR * deltaMmRightEncoderStep;
#ifdef FOUR_WHEEL_ODOM
	deltaMmFL = ticksFL * deltaMmEncoderStep;
	deltaMmFR = ticksFR * deltaMmEncoderStep;
	deltaMmCar = (deltaMmFL + deltaMmFR + deltaMmRL + deltaMmRR) / 4.00;
#else
	deltaMmCar = (deltaMmRL + deltaMmRR) / 2.00;
#endif

	//io_printf("%0.3f\t%0.3f\t%d\t%0.3f\n", deltaMmRL, deltaMmRR, sigWhlTicksRL - odometry.whlTicksRL, sampleTime);
	/* calculate new heading */
	/* kleinwinkeln�herung von arcsin((deltaSRearRightMm - deltaSRearLeftMm )/ TRACK_WIDTH_MM)
	 * Fehler ist bei argumenten < 0,38 kleiner als 1%
	 * => deltaRight - deltaRight < 100*/
	deltaYawAngle = (deltaMmRR - deltaMmRL) / TRACK_WIDTH_MM;

	/* calculate new x and y coordinate in cm
	 * use old Angle because u drive that way with the old angle (nearly)*/
	cosOfYawAngle = cosf(odometry.yawAngle);
	sinOfYawAngle = sinf(odometry.yawAngle);

	// save relevant data to odometry object
	odometry.timestamp = actualTime;    // timstamp
	odometry.speed = (deltaMmCar * MM_TO_M) / sampleTime;    // car speed, v = s/t
	odometry.xM += cosOfYawAngle * deltaMmCar * MM_TO_M;
	odometry.yM += sinOfYawAngle * deltaMmCar * MM_TO_M;
	odometry.yawAngle += deltaYawAngle;

	// TODO -> reduced error on cosf and sinf, see function specs
	// mirror at 360�
	if (odometry.yawAngle > (2.0 * PI))
		odometry.yawAngle -= 2.0 * PI;
	else if (odometry.yawAngle < 0)
		odometry.yawAngle += 2.0 * PI;

	// write relevant data to signal pool
	irq_disable();
	*pVehXDistOdom_odom = odometry.xM;
	*pVehYDistOdom_odom = odometry.yM;
	*pVehYawAngOdom_odom = odometry.yawAngle;
	*pVehSpdOdom_odom = odometry.speed;
	irq_enable();
}

