//
// Created by philipp on 25.08.18.
//

#ifndef VELOXPROTOCOL_ODOMETRY_H
#define VELOXPROTOCOL_ODOMETRY_H

#include <cstdint>

namespace veloxProtocol
{

struct Odometry
{
	std::uint32_t odometryTimestamp;
	std::uint32_t steeringAngleTimestamp;
	float speed;
	float xDistance;
	float yDistance;
	float yawAngle;
	float steeringAngle;
};

}

#endif //VELOXPROTOCOL_ODOMETRY_H
