/********************************************************************************************
 * Unit in charge:
 * @file    i2c2.c
 * @author  ledwig
 * $Date:: 2015-04-07 14:30:00 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1131                  $
 * $Author:: ledwig				$
 *
 * ----------------------------------------------------------
 *
 * @brief 	This is the I2C2 peripheral driver, based on interrupt communication.
 *			original source code: http://www.diller-technologies.de/stm32.html#i2c (modified)
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../i2c2.h"

/************* Private typedefs ***********************************************************/
/************* Macros and constants *******************************************************/
/************* Global variables ***********************************************************/
/************* Private static variables ***************************************************/
static volatile uint8_t deviceAddress; /* i2c slave address */

static volatile uint8_t sendDataByte1; /* first data byte to send */
static volatile uint8_t sendDataByte0; /* second data byte to send */

static volatile uint8_t receivedDataByte3; /* first received data byte */
static volatile uint8_t receivedDataByte2; /* 2nd received data byte */
static volatile uint8_t receivedDataByte1; /* 3rd received data byte */
static volatile uint8_t receivedDataByte0; /* 4th received data byte */

static volatile uint8_t i2cDirectionTransf; /* direction of transfer */
static volatile uint8_t i2cByteCounter; /* number of bytes to transfer */
static volatile uint8_t i2cBusyFlag = 0; /* i2c busy flag */

/************* Private function prototypes ************************************************/
/************* Public functions ***********************************************************/
void i2c2_init(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	I2C_InitTypeDef I2C_InitStructure;

	RCC_APB1PeriphClockCmd(I2C2_CLK, ENABLE); /* enable APB1 peripheral clock for I2Cx */
	RCC_AHB1PeriphClockCmd(I2C2_SDA_CLK, ENABLE); /* enable clock for SCL and SDA pins */

	/* setup SCL and SDA pins */
	GPIO_InitStructure.GPIO_Pin = I2C2_SCL_PIN | I2C2_SDA_PIN; /* SCL: PB10; SDA: PB11 */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF; /* set pins to alternate function */
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; /* set GPIO speed */
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; /* set output to push pull */
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL; /* enable no pull up */
	GPIO_Init(GPIOB, &GPIO_InitStructure); /* initialize GPIOB */

	/* connect I2Cx pins to AF */
	GPIO_PinAFConfig(I2C2_SCL_PORT, I2C2_SCL_AF_PIN, I2C2_AF); /* SCL */
	GPIO_PinAFConfig(I2C2_SDA_PORT, I2C2_SDA_AF_PIN, I2C2_AF); /* SDA */

	/* global interrupt controller */
	NVIC_InitStructure.NVIC_IRQChannel = I2C2_EV_IRQn; /* event */
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3; /* priority */
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = I2C2_ER_IRQn; /* error */
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	I2C_DeInit(I2C2_DEV);

	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; /* set address length to 7 bit */
	I2C_InitStructure.I2C_ClockSpeed = 100000; /* 100kHz */
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2; /* 50% duty cycle --> standard */
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C; /* I2C mode */
	I2C_InitStructure.I2C_OwnAddress1 = 0; /* own address, not relevant in master mode */
	I2C_Init(I2C2_DEV, &I2C_InitStructure); /* initialize I2Cx */

	/* enable interrupt sources */
	I2C_ITConfig(I2C2_DEV, I2C_IT_EVT, ENABLE); /* event */
	I2C_ITConfig(I2C2_DEV, I2C_IT_BUF, ENABLE); /* buffer */
	I2C_ITConfig(I2C2_DEV, I2C_IT_ERR, ENABLE); /* error */

	I2C_Cmd(I2C2_DEV, ENABLE); /* enable I2C2 */
}

void i2c2_writeByte(uint8_t address, uint8_t byte0)
{
	while (i2cBusyFlag); /* wait until i2c communication isn't busy anymore */
	while (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_BUSY));
	deviceAddress = address; /* i2c slave address to send */
	sendDataByte0 = byte0; /* second byte to send */
	i2cDirectionTransf = 1; /* i2c master write mode */
	i2cBusyFlag = 1; /* set busy flag */
	i2cByteCounter = 1; /* two bytes to write */
	I2C_GenerateSTART(I2C2_DEV, ENABLE); /* generate start condition (i2c master transmitter) */
}

void i2c2_writeTwoBytes(uint8_t address, uint8_t byte1, uint8_t byte0)
{
	while (i2cBusyFlag); /* wait until i2c communication isn't busy anymore */
	while (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_BUSY));
	deviceAddress = address; /* i2c slave address to send */
	sendDataByte1 = byte1; /* first byte to send */
	sendDataByte0 = byte0; /* second byte to send */
	i2cDirectionTransf = 1; /* i2c master write mode */
	i2cBusyFlag = 1; /* set busy flag */
	i2cByteCounter = 2; /* two bytes to write */
	I2C_GenerateSTART(I2C2_DEV, ENABLE); /* generate start condition (i2c master transmitter) */
}

uint8_t i2c2_readByte(uint8_t address)
{
	while (i2cBusyFlag); /* wait until i2c communication isn't busy anymore */
	while (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_BUSY));
	deviceAddress = address; /* i2c slave address to send */
	i2cDirectionTransf = 0; /* i2c master read mode */
	i2cBusyFlag = 1; /* set busy flag */
	i2cByteCounter = 1; /* four bytes to read from i2c slave */
	I2C_AcknowledgeConfig(I2C2_DEV, ENABLE); /* enable i2c acknowledge config */
	I2C_GenerateSTART(I2C2_DEV, ENABLE); /* generate start condition (i2c master receiver) */

	I2C_AcknowledgeConfig(I2C2_DEV, DISABLE); /* correct timing necessary here: instruct stop before penultimate reading */

	while (i2cBusyFlag);
	return receivedDataByte0;
}

uint16_t i2c2_readTwoBytes(uint8_t address)
{
	while (i2cBusyFlag); /* wait until i2c communication isn't busy anymore */
	while (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_BUSY));
	deviceAddress = address; /* i2c slave address to send */
	i2cDirectionTransf = 0; /* i2c master read mode */
	i2cBusyFlag = 1; /* set busy flag */
	i2cByteCounter = 2; /* four bytes to read from i2c slave */
	I2C_AcknowledgeConfig(I2C2_DEV, ENABLE); /* enable i2c acknowledge config */
	I2C_GenerateSTART(I2C2_DEV, ENABLE); /* generate start condition (i2c master receiver) */

	while (i2cBusyFlag);
	return (receivedDataByte1 << 8) | receivedDataByte0;
}

uint32_t i2c2_readFourBytes(uint8_t address)
{
	while (i2cBusyFlag); /* wait until i2c communication isn't busy anymore */
	while (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_BUSY));
	deviceAddress = address; /* i2c slave address to send */
	i2cDirectionTransf = 0; /* i2c master read mode */
	i2cBusyFlag = 1; /* set busy flag */
	i2cByteCounter = 4; /* four bytes to read from i2c slave */
	I2C_AcknowledgeConfig(I2C2_DEV, ENABLE); /* enable i2c acknowledge config */
	I2C_GenerateSTART(I2C2_DEV, ENABLE); /* generate start condition (i2c master receiver) */

	while (i2cBusyFlag);
	return (receivedDataByte3 << 24) | (receivedDataByte2 << 16) | (receivedDataByte1 << 8) | receivedDataByte0;
}

/************* ISR **********************************************************/

void I2C2_EV_IRQHandler(void)
{
	if (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_SB) == SET) { /* start bit sent */
		if (i2cDirectionTransf) {
			/* STM32 Transmitter */
			I2C_Send7bitAddress(I2C2_DEV, deviceAddress, I2C_Direction_Transmitter);
		} else {
			/* STM32 Receiver */
			I2C_Send7bitAddress(I2C2_DEV, deviceAddress, I2C_Direction_Receiver);
		}
	} else if (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_ADDR) == SET || I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_BTF) == SET) { /* address sent or data byte transfer finished */
		I2C_ReadRegister(I2C2_DEV, I2C_Register_SR1); /* reset flags by reading from status registers */
		I2C_ReadRegister(I2C2_DEV, I2C_Register_SR2);
		if (i2cDirectionTransf) {
			/* STM32 Transmitter */
			if (i2cByteCounter == 2) {
				I2C_SendData(I2C2_DEV, sendDataByte1);
				i2cByteCounter--;
			} else if (i2cByteCounter == 1) {
				I2C_SendData(I2C2_DEV, sendDataByte0);
				i2cByteCounter--;
			} else {
				I2C_GenerateSTOP(I2C2_DEV, ENABLE);
				i2cBusyFlag = 0;
			}
		}
	} else if (I2C_GetFlagStatus(I2C2_DEV, I2C_FLAG_RXNE) == SET) { /* receive buffer not empty */
		/* STM32 Receiver */
		I2C_ReadRegister(I2C2_DEV, I2C_Register_SR1);
		I2C_ReadRegister(I2C2_DEV, I2C_Register_SR2);
		i2cByteCounter--;
		if (i2cByteCounter == 3) {
			receivedDataByte3 = I2C_ReceiveData(I2C2_DEV);
		} else if (i2cByteCounter == 2) {
			receivedDataByte2 = I2C_ReceiveData(I2C2_DEV);
		} else if (i2cByteCounter == 1) {
			I2C_AcknowledgeConfig(I2C2_DEV, DISABLE); /* correct timing necessary here: disable ack before penultimate reading */
			receivedDataByte1 = I2C_ReceiveData(I2C2_DEV);
		} else {
			I2C_GenerateSTOP(I2C2_DEV, ENABLE);
			receivedDataByte0 = I2C_ReceiveData(I2C2_DEV);
			i2cBusyFlag = 0;
		}
	}
}

void I2C2_ER_IRQHandler(void)
{
	I2C_GenerateSTOP(I2C2_DEV, ENABLE); /* release bus by sending stop condition */
	i2cBusyFlag = 0; /* reset busy flag */

	/* clear error flags */
	I2C_ClearFlag(I2C2_DEV, I2C_FLAG_AF); /* acknowledge failure */
	I2C_ClearFlag(I2C2_DEV, I2C_FLAG_ARLO); /* arbitration loss (master) */
	I2C_ClearFlag(I2C2_DEV, I2C_FLAG_BERR); /* bus error */
}

