/** *****************************************************************************************
 * Unit in charge:
 * @file	uart.h
 * @author	Bjoern Hepner
 * $Date:: 2016-04-05 17:36:40 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1934                  $
 * $Author:: reinhard           $
 * 
 * ----------------------------------------------------------
 *
 * @brief The module uart provides a serial communication interface.
 *
 ******************************************************************************************/

#ifndef UART_H_
#define UART_H_

/************* Includes *******************************************************************/
#include <stdlib.h>
#include "brd/startup/stm32f4xx.h"
#include "per/hwallocation.h"
#include "sys/util/ringbuf.h"
#include "app/config.h"


/************* Public typedefs ************************************************************/
/**
 * @enum uart_channel_t
 *
 * Which UART is to be used ?
 *
 * UART1_CH is USART1 and used pin B6 for transmit and pin B7 for receive.
 *
 * UART2_CH is USART2 and used pin A2 for transmit and pin A3 for receive.
 *
 * UART3_CH is USART3 and used pin D8 for transmit and pin D9 for receive.
 *
 * UART4_CH is USART6 and used pin C6 for transmit and pin C7 for receive.
 */
typedef enum {
#ifdef Board
	UART_CH1 = 0, 		/**< USART1 */
	UART_CH2 = 1, 		/**< USART2 */
	UART_CH4 = 3, 		/**< USART6 */
#endif
	UART_CH1 = 0, 		/**< USART1 */ // needed for RX24F
	UART_CH3 = 2, 		/**< USART3 */
#ifdef Auto
	UART_CH5 = 4 		/**< UART5 */
#endif
 } uart_channel_t;

 /**
  * @struct uart_t
  *
  * content from uart for a receive and send ring buffer.
  *
  */
typedef struct {
	uint32_t 		baudrate; 	/**< You can use -> 2400, 4800, 9600, 19200 or 57600 */
	uart_channel_t	channel; 	/**< There are 4 channels to choose from. UART_CH1, UART_CH2, UART_CH3 and UART_CH4. With them, the UART interface is selected. */
	ringbuf_t 		recring;	/**< Ring buffer for the receive port.  */
	ringbuf_t		sendring;	/**< Ring buffer for the send port. */
} uart_t;

/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/
/**
 *
 * @brief Initializes the UART interface.
 * 		  The following UART channels are available: UART_CH1, UART_CH2, UART_CH3, UART_CH4
 *
 * @param [in,out] *pUart - pointer to an initialized UART interface.
 * @param [in] *pRecBuf	- Buffer used to buffer incoming communication of the uart.
 * @param [in] recBufSize - The size of the buffer for incoming communication.
 * @param [in] *pSendBuf	- Buffer used to buffer outgoing communication of the uart.
 * @param [in] sendBufSize - The size of the buffer for outgoing communication.
 *
 * @return 0 - all right. Everything else is an error.
 */
int8_t uart_init(uart_t* pUart, int8_t* pRecBuf, uint32_t recBufSize, int8_t* pSendBuf, uint32_t sendBufSize, uint32_t *pSendBufTimestamp, uint32_t *pRecBufTimestamp);

/**
 * @brief Writes a byte into UART.
 *
 * @param [in,out] *pUart - pointer to an initialized UART interface.
 * @param [in]  data - written data byte to UART.
 *
 * @return 1 for a byte read.
 * @return -1 Error
 */
int8_t uart_writeByte(uart_t *pUart, int8_t data);

/**
 * @brief Writes a String into UART.
 *
 * @param [in,out] *pUart - pointer to an initialized UART interface.
 * @param [in] *pData - written String byte to UART
 * @param [in] size - of the string to be read
 *
 * @return The number of bytes read.
 * @return -1 Error
 */
int8_t uart_writeString(uart_t* pUart, int8_t* pData, uint16_t size);

/**
 * @brief Reads a byte from UART.
 *
 * @param [in,out] *pUart - pointer to an initialized UART interface.
 * @param [out] *pData - the target where the received byte/string is written to
 *
 * @return The number of bytes read.
 */
int8_t uart_readByte(uart_t* pUart, int8_t* pData);

/**
 * @brief Reads a timed byte from UART.
 *
 * @param [in,out] *pUart - pointer to an initialized UART interface.
 * @param [out] *pData - timed data
 *
 * @return The number of timed bytes read.
 */

int8_t uart_readByte_timed(uart_t *pUart, timed_byte_t *pData);

/**
 * @brief Get a string from ring buffer.
 *
 * @param [in,out] *pUart - pointer to an initialized UART interface.
 * @param [out] *pData - the target where the received byte/string is written to
 * @param [in] size - of the string to be read
 *
 * @return 1 for a byte read.
 */
int8_t uart_readString(uart_t* pUart, int8_t* pData, uint16_t size);

#endif /* UART_H_ */
