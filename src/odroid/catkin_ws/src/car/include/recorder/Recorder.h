//
// Created by philipp on 20.06.18.
//

#ifndef CAR_RECORDER_H
#define CAR_RECORDER_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <atomic>
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include <NetworkingLib/Time.h>
#include <camera/AsyncCapture.h>
#include <mutex>

#include "car/WheelTicks.h"
#include "car/SetThrottle.h"
#include "car/SetAngle.h"
#include "car/UssDistance.h"
#include "car/StopRecording.h"
#include "car/RemoteState.h"

namespace car
{

class Recorder : public nodelet::Nodelet
{
public:
	void onInit() override;

	Recorder();

private:
	ros::NodeHandle nh;

	ros::Subscriber wheelTicksSubscriber;
	ros::Subscriber setThrottleSubscriber;
	ros::Subscriber setAngleSubscriber;
	ros::Subscriber ussDistanceSubscriber;
	ros::Subscriber stopRecordingSubscriber;
	ros::Subscriber remoteStateSubscriber;

	float ticksRL{0};
	float ticksRR{0};
	std::mutex ticksMutex;

	std::atomic<float> setThrottle{0.0f};
	std::atomic<float> setAngle{0.0f};
	std::atomic<float> distance{0.0f};

	std::atomic<bool> recording{true};
	std::atomic<bool> recordingSequence{true};

	AsyncCapture capture;
	cv::Mat currFrame;
	std::mutex frameCopyMutex;
	std::atomic<bool> receivedFrame{false};

	std::string recordingsDirectory;
	networking::time::Duration frameTime;

	void wheelTicksCallback(const WheelTicks::ConstPtr msg);

	void setThrottleCallback(const SetThrottle::ConstPtr msg);

	void setAngleCallback(const SetAngle::ConstPtr msg);

	void ussDistanceCallback(const UssDistance::ConstPtr msg);

	void stopRecordingCallback(const StopRecording::ConstPtr msg);

	void remoteStateCallback(const RemoteState::ConstPtr msg);

	void record();

	std::string createRecordingDirectory();

	void writeCsvRecord(std::ofstream & dataFile, const std::string & imgFilename);

	void readConfig();
};

}

#endif //CAR_RECORDER_H
