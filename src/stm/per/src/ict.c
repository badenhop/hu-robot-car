/** *****************************************************************************************
 * Unit in charge:
 * @file	ict.c
 * @author	ledwig
 * $Date:: 2017-12-05 12:29:39 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske           $
 *
 * ----------------------------------------------------------
 *
 * @brief This unit configures an input capture timer triggering on the edges of the rc signal.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "per/ict.h"
#include "dev/rcrec/rcrec.h"
#include "per/irq.h"
#include "per/hwallocation.h"

/************* Private typedefs ***********************************************************/
/************* Macros and constants *******************************************************/
/************* Private static variables ***************************************************/
static void (*thisIctISR)(void);

/************* Private function prototypes ************************************************/
/************* Public functions ***********************************************************/
void ict_init(ict_t* initIct)
{

	thisIctISR = initIct->ISR;

	/******************** GPIO Pin config ****************************************/
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = CPPM_PIN;

	RCC_AHB1PeriphClockCmd(CPPM_PIN_CLOCK, ENABLE);
	GPIO_Init(CPPM_PORT, &GPIO_InitStructure);

	GPIO_PinAFConfig(CPPM_PORT, CPPM_AF_PIN, CPPM_AF);

	/********************** Timer 9 config ****************************************/
	RCC_APB2PeriphClockCmd(CPPM_CLOCK, ENABLE);	/* enable timer clock */

	// Timer config
	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	TIM_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;

	/*	clock divisor - 168MHz / 168 = 1MHz result -> clock period: 1us */
	TIM_InitStructure.TIM_Prescaler = 1680 - 1;
	TIM_InitStructure.TIM_Period = 65535;     /* timeout after 65.5 ms (FFFFh = 16 bit) */
	TIM_InitStructure.TIM_Period = 3000;
	TIM_TimeBaseInit(CPPM_TIMER, &TIM_InitStructure);

	/* timer interrupt */
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;    /* timer 9 global interrupt */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_Init(&NVIC_InitStructure);

	/* TIM Input Capture Init structure definition */
	TIM_ICInitTypeDef TIM_ICStructure;
	TIM_ICStructure.TIM_Channel = TIM_Channel_1;
	TIM_ICStructure.TIM_ICFilter = 0x0;
	TIM_ICStructure.TIM_ICPolarity = TIM_ICPolarity_Falling;    /* triggering on falling edge */
	TIM_ICStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;			 /* triggering every time */
	TIM_ICStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(CPPM_TIMER, &TIM_ICStructure);


	/*******************************************************************************/

	TIM_ITConfig(CPPM_TIMER, TIM_IT_CC1, ENABLE);    /* enable Timer 9 interrupt source (capture channel1) */
	TIM_ITConfig(CPPM_TIMER, TIM_IT_CC2, DISABLE);    /* disable Timer 9 interrupt source (capture channel2) */
	TIM_ITConfig(CPPM_TIMER, TIM_IT_Update, ENABLE); /* enable Timer 9 interrupt sources (overflow) */

	TIM_Cmd(CPPM_TIMER, ENABLE);    /* enable timer */
}

/************* Private functions **********************************************************/
void TIM1_BRK_TIM9_IRQHandler(void)
{
	thisIctISR() ;

	TIM_ClearITPendingBit(CPPM_TIMER, TIM_IT_CC1);
	TIM_ClearITPendingBit(CPPM_TIMER, TIM_IT_CC2);
	TIM_ClearITPendingBit(CPPM_TIMER, TIM_IT_Update);
}
