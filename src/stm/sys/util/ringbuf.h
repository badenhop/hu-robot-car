/** *****************************************************************************************
 * Unit in charge:
 * @file	ringbuf.h
 * @author	Bjoern Hepner
 * $Date:: 2015-02-24 11:03:01 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1336                  $
 * $Author:: hepner             $
 * 
 * ----------------------------------------------------------
 *
 * Provides the ring buffer.
 *
 ******************************************************************************************/

#ifndef RINGBUF_H_
#define RINGBUF_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"


/************* Public typedefs ************************************************************/
/**
 * @struct ringbuf_t
 *
 * content from everyone ring buffers.
 *
 */
typedef struct
{
	int32_t size;	/**< Size of the ring buffer. */
	int32_t rIndex;	/**< Read index */
	int32_t wIndex;	/**< Write index */
	int32_t count;	/**< Counter for read and write */
	int8_t *pData;	/**< Pointer from receive data or transmit data. */
	uint32_t *pTimestamp; /**< Timestamp from "sched_getSysTime()" */
} ringbuf_t;


/**
 * @struct ringbuf_init_t
 *
 * init Values from ring Buffers
 */
typedef struct
{
	int16_t size;		/**< Initialization size of the ring buffer. */
	int8_t *memory;		/**< Initialization memory of the ring buffer. */
	uint32_t *timestamp; /**< Initialization memory of the ring buffer. */
} ringbuf_init_t;


typedef struct
{
		uint8_t data;
		uint32_t timestamp;
} timed_byte_t;

/************* Macros and constants *******************************************************/
#ifndef NULL
#define NULL 0 			/**< used for pointer */
#endif

/************* Public function prototypes *************************************************/
/**
 * @brief initialize the ring buffer
 *
 * @param  [in,out] *pRing contend from everyone ring buffers
 * @param  [in] *pInitData init Values from ring Buffers
 *
 * @return 0 - all right
 */
int8_t ringbuf_init(ringbuf_t *pRing, ringbuf_init_t *pInitData);

/**
 * @brief write one character into the ring buffer
 *
 * @param  [in,out] *pRingBuf structure from ring buffer
 * @param  [in] data with the one charcter
 *
 * @return RINGBUF_NOBUF_INIT
 * @return 0 - Error  not enough space in ringBuffer
 * @return 1 - write a character
 *
 */
int8_t ringbuf_write(ringbuf_t *pRingBuf, int8_t data);

/**
 * @brief write a string from characters into the ring buffer
 *
 * @param [in,out] *pRingBuf structure from ring buffer
 * @param [in] *pData pointer to the address of the character strings
 * @param [in] size number of characters to be written
 *
 * @return RINGBUF_NOBUF_INIT
 * @return 0 - Error  not enough space in ringBuffer
 * @return size - number of characters written
 *
 */
int32_t ringbuf_writeString(ringbuf_t *pRingBuf, int8_t *pData, uint16_t size);


/**
 * @brief read one character from the ring buffer
 *
 * @param  [in,out] *pRingBuf structure from ring buffer
 * @param  [out] *pData Pointer to the character into the ring buffer
 *
 * @return RINGBUF_NOBUF_INIT
 * @return 0 - it was send all
 * @return 1 - read a character
 *
 */
int8_t ringbuf_read(ringbuf_t *pRingBuf, int8_t *pData);

/**
 * @brief read one character from the timed ring buffer
 *
 * @param  [in,out] *pRingBuf structure from ring buffer
 * @param  [in,out] **pData timed data byte
 * @param  [out] *pData Pointer to the character into the ring buffer
 *
 * @return RINGBUF_NOBUF_INIT
 * @return 0 - it was send all
 * @return 1 - read a character
 *
 */
int8_t ringbuf_read_timed(ringbuf_t *pRingBuf, timed_byte_t *pData);

/**
 * @brief read a string from the ring buffer
 *
 * @param [in,out] *pRingBuf structure from ring buffer
 * @param *pData Timestamp
 *
 * @return RINGBUF_NOBUF_INIT
 * @return 0 - it was send all
 * @return size - number of characters read
 *
 */
int32_t ringbuf_readString(ringbuf_t *pRingBuf, int8_t *pData, uint16_t size);

#endif /* RINGBUF_H_ */
