#ifndef LEDDEBUG
#define LEDDEBUG

void debugLedInit();

void debugLedOn(void);

void debugLedOff(void);

#endif