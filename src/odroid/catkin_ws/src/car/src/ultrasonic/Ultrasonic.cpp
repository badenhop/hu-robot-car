#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <utils/Config.h>
#include "NetworkingLib/Timer.h"
#include "ultrasonic/Ultrasonic.h"

#include "car/UssDistance.h"

PLUGINLIB_EXPORT_CLASS(car::Ultrasonic, nodelet::Nodelet
);

namespace car
{

Ultrasonic::Ultrasonic()
{
    sensor = UltrasonicSensor::create(net, readDevId());
}

void Ultrasonic::onInit()
{
    ussDistancePublisher = nh.advertise<UssDistance>("UssDistance", 1);
    sensor->start(
        [this](auto distance)
         {
             UssDistance msg;
             msg.distance = distance;
             msg.timestamp = ros::Time::now();
             ussDistancePublisher.publish(msg);
         });
}

int Ultrasonic::readDevId()
{
	auto j = config::open();
	std::stringstream ss;
	ss << std::hex << j.at("ultrasonicDeviceId").get<std::string>();
	int devId;
	ss >> devId;
	return devId;
}

}
