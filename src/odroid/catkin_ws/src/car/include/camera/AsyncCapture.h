//
// Created by philipp on 17.08.18.
//

#ifndef CAR_ASYNCCAPTURE_H
#define CAR_ASYNCCAPTURE_H

#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>

#include <gst/gst.h>
#include <gst/app/gstappsink.h>

namespace car
{

class AsyncCapture
{
public:
	using OnFrameCapturedCallback = std::function<void(cv::Mat & frame)>;

	explicit AsyncCapture(int frameWidth = 744, int frameHeight = 480, int frameRate = 60);

	~AsyncCapture();

	AsyncCapture(const AsyncCapture &) = delete;

	AsyncCapture(AsyncCapture && other) noexcept;

	AsyncCapture & operator=(const AsyncCapture &) = delete;

	AsyncCapture & operator=(AsyncCapture && other) noexcept;

	void start(const OnFrameCapturedCallback & callback);

	void stop();

private:
	int frameWidth, frameHeight, frameRate;

	OnFrameCapturedCallback onFrameCapturedCallback;

	GstElement * pipeline = nullptr;
	GstElement * source = nullptr;
	GstElement * inputCaps = nullptr;
	GstElement * queue = nullptr;
	GstElement * bayer2rgb = nullptr;
	GstElement * convert = nullptr;
	GstElement * appsink = nullptr;

	static GstFlowReturn appsinkFrameCallback(GstAppSink * appsink, gpointer data);

	void createPipeline();

	void ensureReadyState();
};

}

#endif //CAR_ASYNCCAPTURE_H
