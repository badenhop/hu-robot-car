//
// Created by philipp on 23.08.18.
//

#ifndef CAR_REMOTECONTROLMESSAGERECEIVER_H
#define CAR_REMOTECONTROLMESSAGERECEIVER_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include "NetworkingLib/DatagramReceiver.h"

#include "RemoteControlMessage.h"

namespace car
{

class RemoteControlMessageReceiver : public nodelet::Nodelet
{
public:
	void onInit() override;

private:
	ros::NodeHandle nh;

	ros::Publisher remoteThrottlePublisher;
	ros::Publisher remoteAnglePublisher;
	ros::Publisher remoteStatePublisher;

	networking::Networking net;

	networking::message::DatagramReceiver<RemoteControlMessage>::Ptr messageReceiver;

	void receiveMessages();

	void handleMessage(const RemoteControlMessage & message);
};

}

#endif //CAR_REMOTECONTROLMESSAGERECEIVER_H
