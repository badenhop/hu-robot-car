//
// Created by philipp on 16.10.18.
//

#include <pluginlib/class_list_macros.h>
#include "helloDrive/HelloDrive.h"
#include <thread>
#include <ros/console.h>

#include "car/SetThrottle.h"
#include "car/SetAngle.h"

// Don't forget this definition when using nodelets!
PLUGINLIB_EXPORT_CLASS(car::HelloDrive, nodelet::Nodelet);

namespace car
{

void HelloDrive::onInit()
{
	setThrottlePublisher = nh.advertise<SetThrottle>("SetThrottle", 1);
	setAnglePublisher = nh.advertise<SetAngle>("SetAngle", 1);

	// printing
	ROS_DEBUG("Hello Drive!");

	// simply starts a thread which performs some work
	std::thread runner{
		[&]
		{
			for (;;)
			{
				// To tell the car at which speed it has to drive we will publish a SetThrottle ROS message.
				SetThrottle setThrottleMsg;
				setThrottleMsg.throttle = 1.0f; // meter per second
				setThrottlePublisher.publish(setThrottleMsg);

				// By publishing a SetAngle message, we communicate the steering angle.
				SetAngle setAngleMsg;
				setAngleMsg.angle = -10.0f; // degrees (negative values: left turn, positive values: right turn)
				setAnglePublisher.publish(setAngleMsg);

				// Just waiting one second, why not...
				using namespace std::chrono_literals;
				std::this_thread::sleep_for(1s);
			}
		}};
	// keeps the thread running even after destruction
	runner.detach();
}

}