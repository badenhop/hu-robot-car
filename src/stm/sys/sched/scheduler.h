/** *****************************************************************************************
 * Unit in charge:
 * @file	scheduler.h
 * @author	Bernhard Kaiser <bernhard.kaiser@berner-mattner.com>
 * @author 	Hauke Petersen	<mail@haukepetersen.de>
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief 	A simple scheduler based on periodic tasks with fixed time-slots.
 *
 ******************************************************************************************/


#ifndef SCHEDULER_H_
#define SCHEDULER_H_

/************* Includes *******************************************************************/
#include <stdint.h>

/************* Includes *******************************************************************/


/************* Public typedefs ************************************************************/


/************* Macros and constants *******************************************************/
#ifndef SCHED_MAX_TASKS
#define SCHED_MAX_TASKS			32				// the scheduler can run 32 tasks at max
#endif

// default time-level definitions
#ifndef SCHED_NUMOF_TIMELEVELS
#define SCHED_NUMOF_TIMELEVELS 	8				// Currently implemented number of time levels
#endif											// (8 assumed by some of the following functions!!!)
#ifndef SCHED_DEFAULT_TIMELEVEL_0
#define SCHED_DEFAULT_TIMELEVEL_0	1			// time-level 0: 1ms
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_1
#define SCHED_DEFAULT_TIMELEVEL_1	5
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_2
#define SCHED_DEFAULT_TIMELEVEL_2	10
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_3
#define SCHED_DEFAULT_TIMELEVEL_3	20
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_4
#define SCHED_DEFAULT_TIMELEVEL_4	500
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_5
#define SCHED_DEFAULT_TIMELEVEL_5	1000
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_6
#define SCHED_DEFAULT_TIMELEVEL_6	5000
#endif
#ifndef SCHED_DEFAULT_TIMELEVEL_7
#define SCHED_DEFAULT_TIMELEVEL_7	10000
#endif

// Responses of taskStatus()
#define SCHED_TASK_EXECUTING		 1
#define SCHED_TASK_KILLED	 		 0
#define SCHED_TASK_NOT_EXIST		-1

/************* Public function prototypes *************************************************/
/**
 * @brief 	Initialize the scheduler.
 *
 * The function initializes the scheduler and sets up all required data structures
 * and state variables.
 * TODO only set up the scheduler here and take out the start code
 *
 * @return 0 on success, -1 otherwise
 */
int8_t sched_init(void);

/**
 * @brief	Start the scheduler
 *
 * The function starts the scheduler. From the starting point all activated tasks will be scheduled.
 *
 * @return 0 on success, -1 otherwise
 */
int8_t sched_start(void);

/**
 * @brief	Stop the scheduler, no task will be scheduled
 *
 * When the scheduler is stopped, no tasks will be scheduled anymore, independent of if there are PENDING
 * tasks.
 *
 * @return 0 on success, -1 otherwise
 */
int8_t sched_stop(void);

/**
 * @brief	Get the current system time
 *
 * @return the current system time in system timer ticks
 */
uint32_t sched_getSchedTime(void);

/**
 * @brief 	!DEPRECATED! Get the task handle for the task with the given task ID
 *
 * TODO: See if this function need actually to be implemented.
 * 			Hauke: I think it is not needed, and by not implementing it the sched_task_t structure
 * 			can be kept private to the scheduler module.
 *
 * @param [in]	tid		Get the task handle for the task with this id
 * @return the task handle of the task with the given id, NULL if no task with that ID exists
 */
//sched_task_t sched_getTask(uint8_t tid);

/**
 * @brief	Checks if a task with the given task ID exists and if its activated.
 *
 * @param[in]	tid		A task id to check for
 * @return 1 if the task is active, 0 if its suspended, -1 if no task with the given ID exists
 */
int8_t sched_taskStatus(uint8_t tid);

/**
 * @brief	Active the task with the given task ID
 *
 * The task with the id tid is activated. This means that the task's status is set to RUNNING
 * and the task will be executed depending on it's timelevel.
 *
 * @param[in]	tid		The task id of the task to activate
 * @return 0 on success, -1 otherwise
 */
int8_t sched_activateTask(uint8_t tid);

/**
 * @brief	Suspend a task so it will not longer be scheduled
 *
 * @param[in]	tid		The ID of the task to suspend
 * @return 0 on success, -1 otherwise
 */
int8_t sched_suspendTask(uint8_t tid);

/**
 * @brief	Register a task with the scheduler
 *
 * Before it is possible to schedule a task, the task needs to be defined. This is done by calling
 * the sched_registerTask function. It needs the pointer to the task's function, a clear text
 * descriptor and the intended time-level for the task.
 *
 * @param[in]	fnct		The function containing the task's code
 * @param[in]	descriptor	A clear text name for the task, only needed for debugging purposes
 * @param[in]	timelevel	The intended timelevel for the task, see SHED_TIMELEVEL_X
 * @return the newly created task's tid, negative value on error
 */
int8_t sched_registerTask(void(*fnct)(void), const char *descriptor, uint8_t timelevel);

/**
 * @brief	Set the interval time for a given time-level
 *
 * The scheduler works on a fixed number of given time-levels. For each of these time-levels the period
 * can be specified. The value is a multiple of 1ms.
 *
 * @param[in]	timelevel	The time-level to change
 * @param[in]	period		The new timing interval
 * @return 0 on success, -1 otherwise
 */
int8_t sched_setTimelevel(uint8_t timelevel, uint16_t period);


#endif /* SCHEDULER_H_ */
