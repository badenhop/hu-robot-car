/** *****************************************************************************************
 * Unit in charge:
 * @file	pwm.h
 * @author	wandji-kwuntchou
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief   The module generates a PWM signal to the selected channel. It provides function to initialize the PWM channels wherein the PWM-frequency ( minimum 50 Hz) is chosen.
 *
 *          You also have the opportunity to choose the mode, the PWM will operate with. During the initialization the user has two options to select:
 *          1. By setting the flexOffFrequency on FALSE the best possible resolution (10 000) is used. The best accurate frequency can be set up maximal to 8.4 kHz
 *          Resulting accurate frequencies : [8.4 kHz, 4.2 kHz, 2.8 kHz, 2.1 kHz, 1.68 kHz, 1.4 kHz, 1.2 kHz and 1.05 kHz....]
 *          2. By setting the flexOffFrequency on TRUE the frequency (max 20kHz) can arbitrarily be set to the detriment of resolution.
 *
 * 		    The PWM module provide 4 channels which operate with same frequency. Any channel can be switched on or off and the respective duty cycle (0.0000 ... 1.)
 * 		    can be set individually.
 *
 * @note    The four PWM channels use the PWM_TIMER (in "dri/hwallocation.h" ) to clock the signal. To generate a PWM signal, firstly initialize the PMW channels which is done by #pwm_init().
 *          then set the Duty Cycle with #pwm_setDutyCycle(). If a PWM channel must be disabled, this occurs with #pwm_disable(). To enable the disabled channel,
 *          the function #pwm_enable() must be called.
 *
 * Example Code for using PWM signal.
 *
 *  @code
 *	pwm_t pwm;
 *	pwm.flexOfFrequency = FALSE;       //pwm.resolution = 10000;
 *	pwm.frequency = 8400;         // 8.4 kHz
 *	pwm.mode = centeraligned;     // Center aligned mode
 *
 *  pwm_init(&pwm);
 *  pwm_setDutycycle(&pwm, PWM_CH1, 0.5);
 *  int32_t i = 0,j = 0;
 *  while(1){
 *  i++;
 *		if (i > 10000000) {
 *		pwm_disable(PWM_CH1);
 *			j++;
 * 		}
 *
 * 		if (j == 10000000) {
 *			pwm_enable(&pwm1, PWM_CH1);
 * 			i = 0;
 *			j = 0;
 *		}
 *
 *	}
 * @endcode
 *
 ******************************************************************************************/

#ifndef PWM_H_
#define PWM_H_

/************* Includes *******************************************************************/
#include "per/hwallocation.h"
#include "app/config.h"


#ifdef PWM_EN

/************* Public typedefs ************************************************************/
/**
 * @enum pwm_channel_t
 *
 * The different PWM Channels which are clocked by the PWM_TIMER and mapped to the following GPIO pins.
 *
 */
typedef enum {
#ifdef PWM_CH1_EN
	PWM_CH1 = 0,
#endif
#ifdef PWM_CH2_EN
	PWM_CH2,
#endif
#ifdef PWM_CH3_EN
	PWM_CH3,
#endif
#ifdef PWM_CH4_EN
	PWM_CH4,
#endif
	PWM_NUM_CHANNELS  /**< Used to calculate the amount of PWM channels  */
} pwm_ch_t;

/**
 * @enum pwm_mode_t
 *
 * The different PWM operating counter modes .
 *
 */
typedef enum {
	leftaligned=1,            /**< PWM counter mode = center aligned1    */
	rightaligned,             /**< PWM counter mode = center aligned2    */
	centeraligned             /**< PWM counter mode = center aligned3    */
} pwm_mode_t;

/**
 * @struct pwm_t
 *
 * content the configuration parameter for a PWM signal
 *
 */
typedef struct {
	uint32_t 		frequency; 	        				  /**< Frequency from the PWM unit in Hz 2Hz/50Hz/2kHz/8kHz                   */
	uint16_t 		resolution;	        			      /**< Resolution from the PWM unit. 100/1000/10000                           */
	bool_t          flexOfFrequency;     			      /**< choose the PWM operating option                                        */
	pwm_mode_t      mode;                			      /**< choose the PWM operating mode                                          */
	float32_t       saveDutyCycle[PWM_NUM_CHANNELS];      /**< save the current duty cycle of the PWM channel which has been disabled */
} pwm_t;

/************* Macros and constants *******************************************************/
/*
 * \defgroup errMessPwm PWM error messages
 *
 */
#define PWM_INIT_FAILED 		    -1         /**< Initialization of the PWM failed. */
#define PWM_ENABLE_FAILED 		    -2         /**< Enable of the PWM failed.         */
#define PWM_DISABLE_FAILED		    -3		   /**< Disable of the PWM failed.        */
#define PWM_SETDUTYCYCLE_FAILED		-4		   /**< Disable of the PWM failed.        */
#define TRUE                         1         /**< Boolean true                      */
#define FALSE                        0         /**< Boolean false                     */





/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the four PWM channels. During the initialization, the duty cycle of the four channels is set to 0.0.
 * That mean that each PWM channel have a low signal (0V).
 *
 * @param [in,out] *pPwm - Pointer to initialize the PWM module.
 * selected parameter are : Resolution from PWM (TIM_Period), frequency (TIM_Prescaler), flexOfFrequency (TRUE or FALSE)
 * and the PWM operating mode. saveDutyCycle isn't used during the initialization
 *
 * @return PWM_INIT_FAILED in case of error or 0 in case of a successful initialization
 */
int8_t pwm_init(pwm_t* pPwm);

/**
 * @brief Set the duty cycle of the PWM channel. This function also save the duty cycle set in the parameter saveDutyCycle[].
 * After the call of this function the PWM channel have a high signal (about 3V) with the desired duty cycle
 *
 * @param [in,out] *pPwm  - Pointer to a pwm_t structure. This parameter is used to save the duty cycle.
 * @param [in] pwmChannel - The PWM channel on which the duty cycle is set
 * @param [in] dutyCycle  - The duty cycle to be set
  *
 * The duty cycle can be set in a range from 0.0000 to 1.0
 *
 * @return PWM_SETDUTYCYCLE_FAILED in case of error or 0 if the duty cycle has been successfully set
 */
int8_t pwm_setDutyCycle(pwm_t* pPwm, pwm_ch_t pwmChannel, float32_t dutyCycle);

/**
 * @brief Enable the PWM Channel. Before this function is called, the channel must be first disabled. The saved duty cycle (in pwm_setDutyCycle)
 * is then updated and reloaded. The PWM channel have a high signal with the reloaded duty cycle.
 *
 * @param [in,out] *pPwm  - Pointer to a pwm_t structure. This parameter is used to update the duty cycle by enabling
 * @param [in] pwmChannel - The PWM channel to enable
 *
 * @return PWM_ENABLE_FAILED in case of error and 0 in case of a successful enable
 */
int8_t pwm_enable(pwm_t* pPwm, pwm_ch_t pwmChannel);

/**
 * @brief disable the PWM channels. Before this function is called, the duty cycle ought to be set previously. Disable here mean that the PWM channel
 * have a duty cycle of 0.0 which indicate a low signal (0 V) .
 *
 * @param [in] pwmChannel - The PWM channel to disable
 *
 * @return PWM_DISABLE_FAILED in case of error or 0 if the channel is disabled
 */
int8_t pwm_disable(pwm_ch_t pwmChannel);

#endif /* PWM_EN*/
#endif /* PWM_H_ */
