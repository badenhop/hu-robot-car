/** *****************************************************************************************
 * Unit in charge:
 * @file	servo.h
 * @author	killer
 * $Date:: 2015-10-19 16:22:22 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1741                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief This module is for controlling of two servo devices.
 *        It supports the initialization, the activation and
 *        the deactivation of the servo channels and it can set
 *        a desired position angle of a servo from the signal pool.
 *
 ******************************************************************************************/

#ifndef SERVO_H_
#define SERVO_H_

/************* Includes *******************************************************************/

#include "per/pwm.h"
#include "per/irq.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants ******************************************************/

/************* Public function prototypes *************************************************/

void servo_updateMotorTrq(float u);

/**
 * @brief Initializes the two servo ports (Servo1 and Servo2) by activating the PWM Port and setting the initial Angle of the servo position.
 *
 * @return Indicates Out-Of-Range Failure by returning value 3 for Servo1 and value 4 for Servo2. With return value 0, the initialization was ok.
 */
int8_t servo_init(void);

/**
 * @brief Enables PWM Channel with Servo1
 *
 * @return 0
 */
int8_t servo_enablePwmSrv1(void);

/**
 * @brief Disables PWM Channel with Servo1
 *
 * @return 0
 */
int8_t servo_disablePwmSrv1(void);

/**
 * @brief Setting the actual servo angle for servo1 from signal pool
 *
 * @return Indicates Out-Of-Range Failure by returning value 3. With return value 0, the update was ok.
 */
int8_t servo_updateAngleSrv1(void);

/**
 * @brief Enables PWM Channel with Servo2
 *
 * @return 0
 */
int8_t servo_enablePwmSrv2(void);

/**
 * @brief Disables PWM Channel with Servo2
 *
 * @return 0
 */
int8_t servo_disablePwmSrv2(void);

/**
 * @brief Setting the actual servo angle for servo2 from signal pool
 *
 * @return Indicates Out-Of-Range Failure by returning value 3. With return value 0, the update was ok.
 */
int8_t servo_updateAngleSrv2(void);

/**
 * @brief Save the value of the servo-angle in the signal pool
 *
 * @return  in case of error or 0 in case of successful initialization
 */
void servo_srvAngCalc(void);

#endif /* SERVO_H_ */
