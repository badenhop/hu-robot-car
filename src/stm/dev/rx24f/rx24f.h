/** *****************************************************************************************
 * Unit in charge:
 * @file	rx24f.h
 * @author	fklemm
 * $Date:: 2017-11-13 14:05:29 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2195                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief Device that controls the communication between this board and the smart Servo (RX24F) via UART.
 *
 ******************************************************************************************/

#ifndef RX24F_H_
#define RX24F_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "per/uart.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

extern volatile uint8_t uartSendBufferEmpty;
extern volatile uint8_t uartFirstReceivedByte;
;
/************* Public function prototypes *************************************************/
/**
 *	@brief The init function needs to be called during the initialization of the system.
 *	It sets all local variables to their initial values.
 */
int8_t rx24f_init(void);

/**
 *	@brief The step function needs to be called cyclically by the scheduler. It evaluates
 *	the input signals and writes the determined values in the signal pool.
 */
void rx24f_step(void);

#endif /* RX24F_H_ */
