/** *****************************************************************************************
 * Unit in charge:
 * @file	rcrec.h
 * @author	ledwig
 * $Date:: 2017-12-05 12:29:39 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske           $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit decodes the rc signal into channel times and stores them via external pointers into the signalpool.
 * Decoding is managed by a statemachine handling four different states: INITIALIZED, SYNC, REC and ERROR. Init Function
 * configures a timer, which is used to determine the detected capture times for the channels as well as the sync period.
 * Each capture is detected, the timer interrupt handler is called and the current timer value is used for further pulse
 * calculation.
 * INITIALIZED state is default state and entered at first by starting the program.
 * SYNC state is used for synchronization with sync pulse and is only leaved into REC state if the sync pulse is recognized.
 * REC state is used to determine the correct pulse times und stores the channel times into signalpool if a sync pulse is detected.
 * ERROR state is to clear up the local and global (signalpool) channel values in case of an error (e.g. undefined pulse time).
 *
 *
 ******************************************************************************************/


#ifndef RCREC_H_
#define RCREC_H_

/************* Includes *******************************************************************/
#include "per/irq.h"
/************* Public typedefs ************************************************************/

#define MAX_PERIOD_CHANNEL 2050
#define MIN_PERIOD_CHANNEL 950
#define RC_CHANNEL_HIGH (MAX_PERIOD_CHANNEL * 0.95)
#define RC_CHANNEL_LOW (MIN_PERIOD_CHANNEL * 1.05)

/************* Macros and constants *******************************************************/

enum {
	RCREC_STATE_INACTIVE = 0x00,
	RCREC_STATE_REC = 0x01,
	RCREC_STATE_ERROR = 0x02,
	RCREC_STATE_SYNC = 0x03
};
/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the rcrec interface and timer interface. Starts the reception of rc signal values.
 *
 * @return - 0 all right.
 */
int8_t rcrec_init(void);

/**
 * @brief function to be called in the isr of the input capture timer interrup handler.
 * It runs the statemachine interface with 4 states: INITIALIZED, SYNC, REC and ERROR.
 *
 * @return - 0 all right.
 */
int8_t rcrec_isr(void);

#endif /* RCREC_H_ */
