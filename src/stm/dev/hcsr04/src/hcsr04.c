/********************************************************************************************
 * Unit in charge:
 * @file    hcsr04.c
 * @author  ledwig
 * $Date:: 2015-04-07 14:30:00 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1131                  $
 * $Author:: ledwig				$
 *
 * ----------------------------------------------------------
 *
 * @brief 	This is the device driver of the HC-SR04 ultrasonic devices. This device driver
 * 			uses the I2C bus driver for communication. This driver only works in combination
 * 			with the implemented VHDL I2C Slave design, because the HC-SR04 sensors are not
 * 			capable to use I2C bus themselves.
 *
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "dev/hcsr04/hcsr04.h"
#include "math.h"
#include "sys/util/io.h"
#include "sys/paramsys/paramsys.h"

/************* Private typedefs ***********************************************************/
/************* Macros and constants *******************************************************/
#define ARRAY_MAX 5		/* number of stored distance values into arrays */
#define MIN_RANGE 4		/* range check of read values */
#define MAX_RANGE 85

#define DEVICE_0 		/*defines to select certain devices to be read from */
//#define DEVICE_1
//#define DEVICE_2
//#define DEVICE_3
//#define DEVICE_4
//#define DEVICE_5
//#define DEVICE_6
//#define DEVICE_7

/************* Global variables ***********************************************************/
/* parameter */
int32_t* pI2cSlaveAdr_ultrdist; 		/* I2C slave address of FPGA */
float32_t* pUSdistCalibr_ultrdist;
int32_t* pUsdistTemp_ultrdist;			/* temperature [degree celsius] as parameter to calculate sound velocity in air */

/* distance value signals */
int16_t *pDistSensor0_ultrdist;
int16_t *pDistSensor1_ultrdist;
int16_t *pDistSensor2_ultrdist;
int16_t *pDistSensor3_ultrdist;
int16_t *pDistSensor4_ultrdist;
int16_t *pDistSensor5_ultrdist;
int16_t *pDistSensor6_ultrdist;
int16_t *pDistSensor7_ultrdist;

/* valid flag signals for distance values */
bool_t *pValidFlagSensor0_ultrdist; /* 0: valid; 1: invalid */
bool_t *pValidFlagSensor1_ultrdist;
bool_t *pValidFlagSensor2_ultrdist;
bool_t *pValidFlagSensor3_ultrdist;
bool_t *pValidFlagSensor4_ultrdist;
bool_t *pValidFlagSensor5_ultrdist;
bool_t *pValidFlagSensor6_ultrdist;
bool_t *pValidFlagSensor7_ultrdist;

/* timestamp values of devices echo signals */
int16_t *pTimestSensor0_ultrdist;
int16_t *pTimestSensor1_ultrdist;
int16_t *pTimestSensor2_ultrdist;
int16_t *pTimestSensor3_ultrdist;
int16_t *pTimestSensor4_ultrdist;
int16_t *pTimestSensor5_ultrdist;
int16_t *pTimestSensor6_ultrdist;
int16_t *pTimestSensor7_ultrdist;

/************* Private static variables ***************************************************/
static float32_t distanceData = 0; /* merged distance value data word */

/* arrays to store the read distance values. values have 16 bits and don't need to be converted.
 they are already in centimeters (vhdl conversion) */
#ifdef DEVICE_0
static float32_t convertedArray0[ARRAY_MAX] = { 0 };
#endif
#ifdef DEVICE_1
static float32_t convertedArray1[ARRAY_MAX] = { 0 };
#endif
#ifdef DEVICE_2
static float32_t convertedArray2[ARRAY_MAX] = {0};
#endif
#ifdef DEVICE_3
static float32_t convertedArray3[ARRAY_MAX] = {0};
#endif
#ifdef DEVICE_4
static float32_t convertedArray4[ARRAY_MAX] = {0};
#endif
#ifdef DEVICE_5
static float32_t convertedArray5[ARRAY_MAX] = {0};
#endif
#ifdef DEVICE_6
static float32_t convertedArray6[ARRAY_MAX] = {0};
#endif
#ifdef DEVICE_7
static float32_t convertedArray7[ARRAY_MAX] = {0};
#endif

static uint8_t indexArray = 0; /* array index */

/************* Private function prototypes ************************************************/
static int8_t send_command(command_t command, uint8_t value);
static float32_t get_distance(device_address_t device_address, float32_t temperature);
static float32_t get_timestamp(timestamp_dev_address_t timestamp_dev_address);

/************* Public functions ***********************************************************/
void hcsr04_init(void)
{
	i2c2_init(); /* initialize i2c peripheral */
	send_command(START_STOP, 1); /* start condition (enable the extern vhdl components, begin to trigger sensors) */
	send_command(TRIGGER, 1); /* set alternative trigger mode for ultrasonic sensors */

	get_distance(DEVICE0, *pUsdistTemp_ultrdist);
}

void hcsr04_process()
{
	//works only in open drain config!!! (except measure command)

	//	//srf02 get measurement
	//	i2c2_writeTwoBytes(0xE8, 0x00, 0x51);	//srf02 address: 0x08; reg: 0; instruction: 0x51 (get measurement)
	//	delay(65);
	//
	//	i2c2_writeByte(0xE8, 0x02);	//write highbyte
	//	uint8_t highbyte = i2c2_readByte(0xE8);
	//
	//	i2c2_writeByte(0xE8, 0x03);	//write lowbyte
	//	uint8_t lowbyte = i2c2_readByte(0xE8);
	//
	//	uint16_t mergedByte = (highbyte << 8) | lowbyte;
	//
	//	io_printf("%i\n\r", mergedByte);


	//works only in push pull config!!!

	/* increment array index for circular buffer */
	indexArray = (indexArray + 1) % ARRAY_MAX;

	float32_t currentVal = 0; /* current received distance value */
	float32_t sumVal = 0; /* sum of values for average filter */

	uint8_t circular4 = 0;
	uint8_t circular3 = 0;
	uint8_t circular2 = 0;
	uint8_t circular1 = 0;

	/* calculate circular array index */
	if ((indexArray - 4) < 0) {
		circular4 = indexArray - 4 + ARRAY_MAX;
		circular4 %= ARRAY_MAX;
	}
	if ((indexArray - 3) < 0) {
		circular3 = indexArray - 3 + ARRAY_MAX;
		circular3 %= ARRAY_MAX;
	}
	if ((indexArray - 2) < 0) {
		circular2 = indexArray - 2 + ARRAY_MAX;
		circular2 %= ARRAY_MAX;
	}
	if ((indexArray - 1) < 0) {
		circular1 = indexArray - 1 + ARRAY_MAX;
		circular1 %= ARRAY_MAX;
	}

#ifdef DEVICE_0
	/*****************device0**********************/
	currentVal = get_distance(DEVICE0, *pUsdistTemp_ultrdist); /* read distance value of device */
	*pTimestSensor0_ultrdist = get_timestamp(TIMEST_DEV0); /* read timestamp of echo pulse from device 0 and store into signalpool */
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) { /* range check and average filter */
		sumVal = 0;

		sumVal += convertedArray0[circular4]; /* (0-4)%5=1; because (-1*5)+1=-4 (circular buffer) */
		sumVal += convertedArray0[circular3];
		sumVal += convertedArray0[circular2];
		sumVal += convertedArray0[circular1];
		convertedArray0[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor0_ultrdist = convertedArray0[indexArray]; /* write value into signalpool */
		*pValidFlagSensor0_ultrdist = 0; /* value valid */
	} else if (currentVal > MAX_RANGE) { /* out of max range */
		*pDistSensor0_ultrdist = MAX_RANGE;
		*pValidFlagSensor0_ultrdist = 1;
	} else { /* out of min range */
		*pDistSensor0_ultrdist = 0;
		*pValidFlagSensor0_ultrdist = 1;
	}
#endif

#ifdef DEVICE_1
	/*****************device1**********************/
	currentVal = get_distance(DEVICE1, *pUsdistTemp_ultrdist);
	*pTimestSensor1_ultrdist = get_timestamp(TIMEST_DEV1);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray1[circular4];
		sumVal += convertedArray1[circular3];
		sumVal += convertedArray1[circular2];
		sumVal += convertedArray1[circular1];
		convertedArray1[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor1_ultrdist = convertedArray1[indexArray];
		*pValidFlagSensor1_ultrdist = 0;
	} else if (currentVal > MAX_RANGE) {
		*pDistSensor1_ultrdist = MAX_RANGE;
		*pValidFlagSensor1_ultrdist = 1;
	} else {
		*pDistSensor1_ultrdist = 0;
		*pValidFlagSensor1_ultrdist = 1;
	}
#endif

#ifdef DEVICE_2
	/*****************device2**********************/
	currentVal = get_distance(DEVICE2, *pUsdistTemp_ultrdist);
	*pTimestSensor2_ultrdist = get_timestamp(TIMEST_DEV2);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray2[circular4];
		sumVal += convertedArray2[circular3];
		sumVal += convertedArray2[circular2];
		sumVal += convertedArray2[circular1];
		convertedArray2[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor2_ultrdist = convertedArray2[indexArray];
		*pValidFlagSensor2_ultrdist = 0;
	}
	else if (currentVal > MAX_RANGE) {
		*pDistSensor2_ultrdist = MAX_RANGE;
		*pValidFlagSensor2_ultrdist = 1;
	}
	else {
		*pDistSensor2_ultrdist = 0;
		*pValidFlagSensor2_ultrdist = 1;
	}
#endif

#ifdef DEVICE_3
	/*****************device3**********************/
	currentVal = get_distance(DEVICE3, *pUsdistTemp_ultrdist);
	*pTimestSensor3_ultrdist = get_timestamp(TIMEST_DEV3);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray3[circular4];
		sumVal += convertedArray3[circular3];
		sumVal += convertedArray3[circular2];
		sumVal += convertedArray3[circular1];
		convertedArray3[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor3_ultrdist = convertedArray3[indexArray];
		*pValidFlagSensor3_ultrdist = 0;
	}
	else if (currentVal > MAX_RANGE) {
		*pDistSensor3_ultrdist = MAX_RANGE;
		*pValidFlagSensor3_ultrdist = 1;
	}
	else {
		*pDistSensor3_ultrdist = 0;
		*pValidFlagSensor3_ultrdist = 1;
	}
#endif

#ifdef DEVICE_4
	/*****************device4**********************/
	currentVal = get_distance(DEVICE4, *pUsdistTemp_ultrdist);
	*pTimestSensor4_ultrdist = get_timestamp(TIMEST_DEV4);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray4[circular4];
		sumVal += convertedArray4[circular3];
		sumVal += convertedArray4[circular2];
		sumVal += convertedArray4[circular1];
		convertedArray4[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor4_ultrdist = convertedArray4[indexArray];
		*pValidFlagSensor4_ultrdist = 0;
	}
	else if (currentVal > MAX_RANGE) {
		*pDistSensor4_ultrdist = MAX_RANGE;
		*pValidFlagSensor4_ultrdist = 1;
	}
	else {
		*pDistSensor4_ultrdist = 0;
		*pValidFlagSensor4_ultrdist = 1;
	}
#endif

#ifdef DEVICE_5
	/*****************device5**********************/
	currentVal = get_distance(DEVICE5, *pUsdistTemp_ultrdist);
	*pTimestSensor5_ultrdist = get_timestamp(TIMEST_DEV5);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray5[circular4];
		sumVal += convertedArray5[circular3];
		sumVal += convertedArray5[circular2];
		sumVal += convertedArray5[circular1];
		convertedArray5[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor5_ultrdist = convertedArray5[indexArray];
		*pValidFlagSensor5_ultrdist = 0;
	}
	else if (currentVal > MAX_RANGE) {
		*pDistSensor5_ultrdist = MAX_RANGE;
		*pValidFlagSensor5_ultrdist = 1;
	}
	else {
		*pDistSensor5_ultrdist = 0;
		*pValidFlagSensor5_ultrdist = 1;
	}
#endif

#ifdef DEVICE_6
	/*****************device6**********************/
	currentVal = get_distance(DEVICE6, *pUsdistTemp_ultrdist);
	*pTimestSensor6_ultrdist = get_timestamp(TIMEST_DEV6);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray6[circular4];
		sumVal += convertedArray6[circular3];
		sumVal += convertedArray6[circular2];
		sumVal += convertedArray6[circular1];
		convertedArray6[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor6_ultrdist = convertedArray6[indexArray];
		*pValidFlagSensor6_ultrdist = 0;
	}
	else if (currentVal > MAX_RANGE) {
		*pDistSensor6_ultrdist = MAX_RANGE;
		*pValidFlagSensor6_ultrdist = 1;
	}
	else {
		*pDistSensor6_ultrdist = 0;
		*pValidFlagSensor6_ultrdist = 1;
	}
#endif

#ifdef DEVICE_7
	/*****************device7**********************/
	currentVal = get_distance(DEVICE7, *pUsdistTemp_ultrdist);
	*pTimestSensor7_ultrdist = get_timestamp(TIMEST_DEV7);
	if ((currentVal >= MIN_RANGE) && (currentVal <= MAX_RANGE)) {
		sumVal = 0;

		sumVal += convertedArray7[circular4];
		sumVal += convertedArray7[circular3];
		sumVal += convertedArray7[circular2];
		sumVal += convertedArray7[circular1];
		convertedArray7[indexArray] = (sumVal + currentVal) / ARRAY_MAX;
		*pDistSensor7_ultrdist = convertedArray7[indexArray];
		*pValidFlagSensor7_ultrdist = 0;
	}
	else if (currentVal > MAX_RANGE) {
		*pDistSensor7_ultrdist = MAX_RANGE;
		*pValidFlagSensor7_ultrdist = 1;
	}
	else {
		*pDistSensor7_ultrdist = 0;
		*pValidFlagSensor7_ultrdist = 1;
	}
#endif

	/* debug values via uart */
//	io_printf("%i %i\t%i\n\r", *pValidFlagSensor0_ultrdist, *pDistSensor0_ultrdist, *pTimestSensor0_ultrdist); /* note!! use IO after I2C!! */
	//io_printf("%i %i\t%i\n\r", *pValidFlagSensor1_ultrdist, *pDistSensor1_ultrdist, *pTimestSensor1_ultrdist);
}

/************* Private functions **********************************************************/
static int8_t send_command(command_t command, uint8_t value)
{
	i2c2_writeTwoBytes(*pI2cSlaveAdr_ultrdist << 1, command, value);
	return 0;
}

static float32_t get_distance(device_address_t device_address, float32_t temperature)
{
	float32_t rawVal, c;

	i2c2_writeTwoBytes(*pI2cSlaveAdr_ultrdist << 1, device_address, 0); /* write slave address, followed by 2 bytes command for register address to be read from */
	rawVal = i2c2_readFourBytes(*pI2cSlaveAdr_ultrdist << 1); /* read 4 bytes raw distance data */

	/* convert raw value (50 Mhz clock) into ms value */
	rawVal *= 20; /* 20 ns period clock */
	rawVal /= 1000000; /* ns to ms */

	/*
	 * 	calculate distance to object:
	 *
	 * 	v=s/t
	 * 	s=v*t
	 * 	2*s=v*t
	 * 	s=v*t/2		-> t - rawVal (32 bit); v = c (sound velocity)
	 * 	s=c*rawVal/2
	 *
	 *  c [m/s] = c [mm/ms] = 331,5*sqrt(1+(temperature[�C]/273,15))
	 *
	 */

	/* calculate c */
	c = 1 + temperature / 273, 15;
	c = sqrt(c);
	c *= 331, 5;

	distanceData = c * rawVal / 2;
	distanceData /= 10; /* convert into centimeters */
	distanceData *= *pUSdistCalibr_ultrdist; /* calibrate converted distance value depending on real measured distance due to deviations */
	return distanceData; /* return distance */
}

static float32_t get_timestamp(timestamp_dev_address_t timestamp_dev_address)
{
	float32_t timestampVal;

	i2c2_writeTwoBytes(*pI2cSlaveAdr_ultrdist << 1, timestamp_dev_address, 0); /* write slave address, followed by 2 bytes command for register address to be read from */
	timestampVal = i2c2_readFourBytes(*pI2cSlaveAdr_ultrdist << 1); /* read 4 bytes raw distance data */

	/* convert raw value (50 Mhz clock) into ms value */
	timestampVal *= 20; /* 20 ns period clock */
	timestampVal /= 1000000; /* ns to ms */
	return timestampVal;
}

