//
// Created by philipp on 20.06.18.
//

#include <pluginlib/class_list_macros.h>
#include <utils/Config.h>
#include "remoteControl/RemoteControl.h"
#include "car/SetThrottle.h"
#include "car/SetAngle.h"

PLUGINLIB_EXPORT_CLASS(car::RemoteControl, nodelet::Nodelet);

namespace car
{

void RemoteControl::onInit()
{
	readConfig();

	if (controlThrottle)
	{
		remoteThrottleSubscriber = nh.subscribe("RemoteThrottle", 1, &RemoteControl::remoteThrottleCallback, this);
		setThrottlePublisher = nh.advertise<SetThrottle>("SetThrottle", 1);
	}

	if (controlSteeringAngle)
	{
		remoteAngleSubscriber = nh.subscribe("RemoteAngle", 1, &RemoteControl::remoteAngleCallback, this);
		setAnglePublisher = nh.advertise<SetAngle>("SetAngle", 1);
	}
}

void RemoteControl::remoteThrottleCallback(const RemoteThrottle::ConstPtr & msg)
{
	SetThrottle throttleMsg;
	throttleMsg.throttle = msg->throttle;
	setThrottlePublisher.publish(throttleMsg);
}

void RemoteControl::remoteAngleCallback(const RemoteAngle::ConstPtr & msg)
{
	SetAngle angleMsg;
	angleMsg.angle = msg->angle;
	setAnglePublisher.publish(angleMsg);
}

void RemoteControl::readConfig()
{
	auto j = config::open();
	if (j.count("enableThrottleRemoteControl") > 0)
		controlThrottle = j.at("enableThrottleRemoteControl").get<bool>();
	if (j.count("enableSteeringAngleRemoteControl") > 0)
		controlSteeringAngle = j.at("enableSteeringAngleRemoteControl").get<bool>();
}

}