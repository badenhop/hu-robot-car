# HU Robot Car

This repo serves as a template project to quickly get used to the HU Robot Car platform.
It contains software for the STM32, the Odroid XU4 as well as some PC scripts.

## Getting started with the Odroid

The Odroid runs an Ubuntu 16.04 operating system.
Login with the root user and the password "autonom1718".
To connect to eduroam, edit the file /etc/wpa_supplicant/wpa_supplicant.conf and enter your hu-credentials under the fields "identity" and "password".
Reboot and check your IP address using ifconfig for example.
Now you're ready to login via SSH.

## Getting the car to drive

Under the directory /root/repos/hu-robot-car you already find this repository.
Now, cd to /root/repos/hu-robot-car/src/odroid and build the project by running `./build.sh`.
You can also clean it by running `./clean.sh`.
In the odroid directory, run `source catkin_ws/devel/setup.sh`.
Finally, to see the car driving (at constant speed and making a slight left turn), enter the command `roslaunch car helloDrive.launch`.
Please, try this first by lifting the car so the wheels don't touch the ground.
This is only for safety to see that everything is working before you unleash it :stuck_out_tongue_winking_eye:.
If nothing happens, make sure everything is connected including the STM32 being connected to the odroid via the mini-USB cable and the motor's battery being charged and connected.
Don't forget to turn on the power switch of the motor as well.

## Structure and architecture

Before diving into the code, it makes sense to familiarize yourself with the ROS framework since the odroid's software is based on that.
The project already includes code to get all the sensor data which comprises the distance measured by the ultrasonic sensor, the ticks from the wheel encoders (to compute speed and odometry) as well as the image from the camera.
All this can be found in the src/odroid/catkin_ws/src/car package.
The following ROS nodes/nodelets are included:

    - stm (communication with the STM32 over UART and mavlink protocol)
    - ultrasonic (reading the ultrasonic sensor data)
    - remoteControlMessageReceiver (receives messages over the network)
    - remoteControl (uses the remote messages to steer the car)
    - recorder (starts a recording session which creates a new directory in /root/.car/recordings storing the images and the writing the sensor into data.csv)
    - helloDrive (a good starting point)

Note, that there's no explicit node for the camera.
You can use the class AsyncCapture to receive images directly without any overhead (e.g. by transferring it over topics).
Therefore, take a look into the recorder node to see how to use it.

Here's an overview how the nodes are connected through topics.
Each topic has an associated message which is contained in the directory car/msg.

![alt text](doc/ros_architecture.png)

It involves a little bit of code reading, to get an overview of what's going on.
It's probably a good idea to start off with the HelloDrive node and going from there to the recorder node where you can checkout how to access all the sensor data.
You may also check out the documentation of the semseter project "Hochautomatisiertes Fahren":

https://www2.informatik.hu-berlin.de/~hs/Lehre/2017-WS_SP-HAF/index.html
    
## The config.json file

There's a configuration file located at /root/.car/config.json which you can use to avoid recompiling your project everytime you make a slight change.


